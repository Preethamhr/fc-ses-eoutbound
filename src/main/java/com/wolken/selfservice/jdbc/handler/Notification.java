package com.wolken.selfservice.jdbc.handler;

public class Notification {
	
	private Integer id;

	private Integer status;

	private Integer sourceId;

	private Integer eventId;

	private Integer companyId;

	private String emailTo;

	private String emaliCc;
	
	private String content;
	
	private Integer issueId;
	
	private String notificationType;
	
	private Integer createdBy;
	
	private Integer updatedBy;
	
	private String createdDate;
	
	private String updatedDate;
	
	private String subject;



	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}
	
	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	public Integer getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	public String getEmailTo() {
		return emailTo;
	}
	
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	
	public String getEmaliCc() {
		return emaliCc;
	}
	
	public void setEmaliCc(String emaliCc) {
		this.emaliCc = emaliCc;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public Integer getIssueId() {
		return issueId;
	}
	
	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}
	
	public String getNotificationType() {
		return notificationType;
	}
	
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getUpdatedDate() {
		return updatedDate;
	}
	
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	 public String toString() {
	        StringBuilder sb = new StringBuilder("id: ");
	        sb.append(id).append(", status: ").append(status).append(", sourceId: ")
	                .append(sourceId).append(", eventId: ").append(eventId)
	                 .append(", companyId: ").append(companyId)
	                  .append(", emailTo: ").append(emailTo)
	                  .append(", emaliCc: ").append(emaliCc)
	                  .append(", content: ").append(content)
	                  .append(", issueId: ").append(issueId)
	                  .append(", notificationType: ").append(notificationType)
	                   .append(", createdBy: ").append(createdBy)
	                    .append(", updatedBy: ").append(updatedBy)
	                     .append(", createdDate: ").append(createdDate)
	                .append(", updatedDate: ").append(updatedDate)
	                .append(", subject: ").append(subject);
	        return sb.toString();
	    }
	
	}
