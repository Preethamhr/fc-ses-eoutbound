package com.wolken.selfservice.jdbc.handler;

public class ElasticsearchRetrigger {

	private int id;
	private Integer ticketId;
	private String ticketDetails;
	private boolean status;
	private long createdTimestamp;
	private Integer companyId;
	
	
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the ticketId
	 */
	public Integer getTicketId() {
		return ticketId;
	}
	/**
	 * @param ticketId the ticketId to set
	 */
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	/**
	 * @return the ticketDetails
	 */
	public String getTicketDetails() {
		return ticketDetails;
	}
	/**
	 * @param ticketDetails the ticketDetails to set
	 */
	public void setTicketDetails(String ticketDetails) {
		this.ticketDetails = ticketDetails;
	}
	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the createdTimestamp
	 */
	public long getCreatedTimestamp() {
		return createdTimestamp;
	}
	/**
	 * @param createdTimestamp the createdTimestamp to set
	 */
	public void setCreatedTimestamp(long createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
	
	
	
	
}
