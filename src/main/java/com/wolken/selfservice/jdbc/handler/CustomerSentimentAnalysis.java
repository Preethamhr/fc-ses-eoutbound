package com.wolken.selfservice.jdbc.handler;

import java.util.Date;



public class CustomerSentimentAnalysis {
	
	private Integer analysisId;
	
	private Integer issueId;
	
	private String content;
	
	private int status;
	
	private float score;
	
	private float magnitude;
	
	private int analysisResult;
	
	private Date createdDate;
	
	private Date updatedDate;

	public Integer getAnalysisId() {
		return analysisId;
	}

	public void setAnalysisId(Integer analysisId) {
		this.analysisId = analysisId;
	}

	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public float getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(float magnitude) {
		this.magnitude = magnitude;
	}

	public int getAnalysisResult() {
		return analysisResult;
	}

	public void setAnalysisResult(int analysisResult) {
		this.analysisResult = analysisResult;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	
	
	
	
	
	
	
	

}
