package com.wolken.selfservice.jdbc.handler;

import java.util.Date;

public class CsatRequests {
	
	
	private Integer csatRequestId;
	
	private Integer issueId;
	
	private Integer issueThreadId;
	
	private Integer userId;
	
	private Integer channelId;
	
	private Integer companyId;
	
	private String csatFrom;
	
	private String csatTo;
	
	private String csatContent;
	
	private String csatCc;
	
	private String csatBcc;
	
	private String csatSubject;
	
	private Date csatCreationDate;
	
	private Integer csatCreatedBy;
	
	private String errorStacktrace;
	
	private long csatTriggerTime;
	
	private int csatStatus;
	
	
	
	
	

	

	public Integer getCsatRequestId() {
		return csatRequestId;
	}

	public void setCsatRequestId(Integer csatRequestId) {
		this.csatRequestId = csatRequestId;
	}

	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public Integer getIssueThreadId() {
		return issueThreadId;
	}

	public void setIssueThreadId(Integer issueThreadId) {
		this.issueThreadId = issueThreadId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getChannelId() {
		return channelId;
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCsatFrom() {
		return csatFrom;
	}

	public void setCsatFrom(String csatFrom) {
		this.csatFrom = csatFrom;
	}

	public String getCsatTo() {
		return csatTo;
	}

	public void setCsatTo(String csatTo) {
		this.csatTo = csatTo;
	}

	public String getCsatContent() {
		return csatContent;
	}

	public void setCsatContent(String csatContent) {
		this.csatContent = csatContent;
	}

	public String getCsatCc() {
		return csatCc;
	}

	public void setCsatCc(String csatCc) {
		this.csatCc = csatCc;
	}

	public String getCsatBcc() {
		return csatBcc;
	}

	public void setCsatBcc(String csatBcc) {
		this.csatBcc = csatBcc;
	}

	public String getCsatSubject() {
		return csatSubject;
	}

	public void setCsatSubject(String csatSubject) {
		this.csatSubject = csatSubject;
	}

	public Date getCsatCreationDate() {
		return csatCreationDate;
	}

	public void setCsatCreationDate(Date csatCreationDate) {
		this.csatCreationDate = csatCreationDate;
	}

	public Integer getCsatCreatedBy() {
		return csatCreatedBy;
	}

	public void setCsatCreatedBy(Integer csatCreatedBy) {
		this.csatCreatedBy = csatCreatedBy;
	}

	public String getErrorStacktrace() {
		return errorStacktrace;
	}

	public int getCsatStatus() {
		return csatStatus;
	}

	public void setCsatStatus(int csatStatus) {
		this.csatStatus = csatStatus;
	}

	public void setErrorStacktrace(String errorStacktrace) {
		this.errorStacktrace = errorStacktrace;
	}

	public long getCsatTriggerTime() {
		return csatTriggerTime;
	}

	public void setCsatTriggerTime(long csatTriggerTime) {
		this.csatTriggerTime = csatTriggerTime;
	}
	
	
	
	
	
	
	
	
	

}
