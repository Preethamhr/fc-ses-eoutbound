package com.wolken.selfservice.jdbc.handler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SMSRequestRowMapper implements RowMapper<SMSRequests>{

	static String User ="arunkp656";
	static String passwd = "Arunsms100";
	static HttpURLConnection urlconnection;
	
	/*public HttpURLConnection getUrlconnection(){
		URL url;
		try {
			url = new URL("http://api.smscountry.com/SMSCwebservice_bulk.aspx");
			urlconnection = (HttpURLConnection) url.openConnection();
			urlconnection.setRequestMethod("POST");
			urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			urlconnection.setDoOutput(true);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return urlconnection;
	}
	*/
	
	@Override
	public SMSRequests mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		SMSRequests analysis= new SMSRequests();
		try{
			String postData="";
			String retval = "";

			analysis.setSmsRequestId(rs.getInt("sms_request_id"));
			analysis.setCompanyId(rs.getInt("company_id"));
			analysis.setSmsTo(rs.getString("sms_to"));
			analysis.setSmsFrom(rs.getString("sms_from"));
			analysis.setSmsRequestInfo(rs.getString("sms_request_info"));
			analysis.setSmsRequestType(rs.getString("sms_request_type"));
			
			String mobilenumber = analysis.getSmsTo(); 
			String message = analysis.getSmsRequestInfo();
			String sid = "";
			String mtype = "N";
			String DR = "Y";		

			//http://smscountry.com/SMSCwebservice_Bulk.aspx
			postData += "User=" + URLEncoder.encode(User,"UTF-8") + "&passwd=" + passwd + "&mobilenumber=" + mobilenumber + "&message=" + URLEncoder.encode(message,"UTF-8") + "&sid=" + sid + "&mtype=" + mtype + "&DR=" + DR;
			System.out.println("url data params : "+postData);
		
			// If You Are Behind The Proxy Server Set IP And PORT else Comment Below 4 Lines
			//Properties sysProps = System.getProperties();
			//sysProps.put("proxySet", "true");
			//sysProps.put("proxyHost", "Proxy Ip");
			//sysProps.put("proxyPort", "PORT");

		/*	if(urlconnection==null){
			urlconnection=getUrlconnection();
			}*/
				URL url = new URL("http://api.smscountry.com/SMSCwebservice_bulk.aspx");
				urlconnection = (HttpURLConnection) url.openConnection();
				urlconnection.setRequestMethod("POST");
				urlconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				urlconnection.setDoOutput(true);
			
			OutputStreamWriter out = new OutputStreamWriter(urlconnection.getOutputStream());
			out.write(postData);
			out.close();
			BufferedReader in = new BufferedReader(	new InputStreamReader(urlconnection.getInputStream()));
			String decodedString;
			while ((decodedString = in.readLine()) != null) {
				retval += decodedString;
				System.out.println(retval);
				String[] status=retval!=null?retval.split(":"):null;
				if(status!=null && status[0].equals("OK")){
					analysis.setSmsRequestStatus("SUCCESS");
				}else{
					analysis.setSmsRequestStatus("ERROR");
				}
			}
			in.close();
		}catch(Exception e){
			e.printStackTrace();
			analysis.setSmsRequestStatus("ERROR");
		}
		return analysis;
	}

}
