package com.wolken.selfservice.jdbc.handler;


import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.wolken.selfservice.model.ThreadCheckResponse;
import com.wolken.selfservice.util.RestUtil;

public class TwitterAccountHandler {
	
	
	//@Value("${threadCheckUrl}")
	private String threadCheckUrl;
	
//	@Value("${streamingBaseUrl}")
	private String streamingBaseUrl; 
	
	/*public String getStreamingBaseUrl() {
		return streamingBaseUrl;
	}

	public void setStreamingBaseUrl(String streamingBaseUrl) {
		this.streamingBaseUrl = streamingBaseUrl;
	}

	
	public String getThreadCheckUrl() {
		return threadCheckUrl;
	}

	public void setHostIp(String threadCheckUrl) {
		this.threadCheckUrl = threadCheckUrl;
	}*/
	
	
	public void handleMessage(List<TwitterCredentials> twitterCredentials){
		
		Gson g = new Gson(); 
		for(TwitterCredentials twiCredentials : twitterCredentials){
			
			String jsonString = RestUtil.httpGetCall(threadCheckUrl+"twitterAccountPkey="+twiCredentials.getTwitterAccountPkey()+"&userId="+twiCredentials.getCreatedBy());
			
			
			ThreadCheckResponse obj = g.fromJson(jsonString, ThreadCheckResponse.class);
			 
			System.out.println("JSON returned thread status "+obj.isThreadstatus());
			System.out.println("JSON returned s a result "+obj.toString());
			
			
			if(obj.isThreadstatus() == false){
				
				String jsonStreamingOutput = RestUtil.httpGetCall(streamingBaseUrl+"twitterAccountPkey="+twiCredentials.getTwitterAccountPkey()+"&userId="+twiCredentials.getCreatedBy());
				
				System.out.println("After Streaming !! Output >>>> "+jsonStreamingOutput);
				
			}
			
			
		}
		
		
	}

	


}
