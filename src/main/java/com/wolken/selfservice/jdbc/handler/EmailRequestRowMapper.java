package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wolken.selfservice.jdbc.connection.MailStatusProcessorUpdate;

public class EmailRequestRowMapper implements RowMapper<EmailRequest>{

	@Override
	public EmailRequest mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		
		/*emailRequest.setEmail_request_id(rs.getInt("email_request_id"));
		emailRequest.setCompany_id(rs.getInt("company_id"));
		emailRequest.setCreationdate(rs.getString("creationdate"));
		emailRequest.setDoc_full_path(rs.getString("doc_full_path"));
		emailRequest.setEmail_bcc(rs.getString("email_bcc"));
		emailRequest.setEmail_cc(rs.getString("email_cc"));
		emailRequest.setEmail_from(rs.getString("email_from"));
		emailRequest.setEmail_request_info(rs.getString("email_request_info"));
		emailRequest.setEmail_request_status(rs.getString("email_request_status"));
		emailRequest.setEmail_request_type(rs.getString("email_request_type"));
		//emailRequest.setEmail_source(rs.getString("email_source")); removed from self service DB
		emailRequest.setEmail_subject(rs.getString("email_subject"));
		emailRequest.setEmail_to(rs.getString("email_to"));
		emailRequest.setEmail_type(rs.getString("email_type"));
		emailRequest.setError_stacktrace(rs.getString("error_stacktrace"));
		emailRequest.setIssue_id(rs.getInt("issue_id"));
		emailRequest.setUser_id(rs.getInt("user_id"));
		return emailRequest;*/
		
		System.out.println("starting looping row processor");
		MailStatusProcessorUpdate wipProcessor=new MailStatusProcessorUpdate();
		EmailRequest emailRequest = new EmailRequest();
		try {
			emailRequest.setEmailRequestId((Integer) rs.getInt("email_request_id"));
			emailRequest.setCompanyId((Integer) rs.getInt("company_id"));
			emailRequest.setEmailType((String) rs.getString("email_type"));
		//	emailRequest.setEmailRequestTypeId((Integer) rs.getInt("email_request_type_id"));
			emailRequest.setEmailRequestTypeName((String) rs.getString("email_request_type"));
			
			emailRequest.setErrorRetry((Integer) rs.getInt("error_retry"));
			
			String htmlContent=(String)rs.getString("email_request_info");
			if(htmlContent!=null){
				htmlContent = htmlContent.trim().replaceAll("(\r\n|\n)", "<br />");
			}
			emailRequest.setEmailRequestInfo(htmlContent);
			emailRequest.setEmailRequestStatus((Integer) rs.getInt("email_request_status"));
			//mailInfo.setEmail_flag((String) rs.get("email_flag"));

			String email_to = (String) rs.getString("email_to");
			
			
			String emails ="";
			if(email_to!=null && email_to.trim().length()>0){
				if(email_to.contains(";")){
					email_to=email_to.replaceAll(";", ",");
				}
				String email_list [] = email_to.split(",");
				for(String emailStr : email_list){
					if(emails.equals("")){
//						emails= EncryptionUtil.decryptString(emailStr);
						emails= emailStr;
					}else{
//						emails= emails +","+EncryptionUtil.decryptString(emailStr);
						emails= emails +","+emailStr;
					}
					
				}
				
				emailRequest.setEmailTo(emails);
			}
			emailRequest.setEmailFrom((String) rs.getString("email_from"));
			emailRequest.setEmailCC((String) rs.getString("email_cc"));
			emailRequest.setEmailBcc((String) rs.getString("email_bcc"));
			
			
			if(rs.getString("email_subject") != null){
				String subject=(String) rs.getString("email_subject");
				subject=subject.replace("\n", " ");
				subject=subject.replace("\r", " ");
				emailRequest.setEmailSubject(subject);
			}
			if(rs.getString("doc_full_path") != null){
				emailRequest.setEmailAttachments((String)rs.getString("doc_full_path"));
			}
			if(emailRequest.getEmailCC()!=null && emailRequest.getEmailCC().contains(";")){
				String cc=emailRequest.getEmailCC();
				cc=cc.replaceAll(";", ",");
				emailRequest.setEmailCC(cc);
			}
			if(emailRequest.getEmailBcc()!=null && emailRequest.getEmailBcc().contains(";")){
				String bcc=emailRequest.getEmailBcc();
				bcc=bcc.replaceAll(";", ",");
				emailRequest.setEmailBcc(bcc);
			}
			
			wipProcessor=new MailStatusProcessorUpdate();
			wipProcessor.updateMailStatus(emailRequest.getEmailRequestId(),"WIP");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println("ending looping row processor-->"+emailRequest.getEmailTo());
		return emailRequest;
	}

}
