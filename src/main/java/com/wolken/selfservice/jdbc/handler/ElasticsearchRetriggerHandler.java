package com.wolken.selfservice.jdbc.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.wolken.selfservice.util.RestUtil;

public class ElasticsearchRetriggerHandler {


	//@Value("${elasticsearchretriggerurl}")
	private String elasticsearchRetriggerURL;

	public void handleMessage(List<ElasticsearchRetrigger> elasticsearchRetriggers) {
		StringBuilder urlParams=null;
		if(elasticsearchRetriggers!=null && elasticsearchRetriggers.size()>0){
			for(ElasticsearchRetrigger elasticsearchRetrigger : elasticsearchRetriggers){
				urlParams= new StringBuilder();
				urlParams=urlParams.append("issueid=").append(elasticsearchRetrigger.getTicketId())
						.append("&issuedetailsjson=").append(elasticsearchRetrigger.getTicketDetails())
						.append("&companyid=").append(elasticsearchRetrigger.getCompanyId());
				RestUtil.httpPostCall(elasticsearchRetriggerURL, urlParams.toString());
			}
		}
	}
}
