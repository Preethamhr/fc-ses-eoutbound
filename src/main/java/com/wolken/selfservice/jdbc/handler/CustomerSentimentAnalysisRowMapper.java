package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jsoup.Jsoup;
import org.springframework.jdbc.core.RowMapper;

import net.sf.json.JSONObject;

public class CustomerSentimentAnalysisRowMapper implements RowMapper<CustomerSentimentAnalysis>{

	@Override
	public CustomerSentimentAnalysis mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub

		CustomerSentimentAnalysis analysis= new CustomerSentimentAnalysis();
		try{
		analysis.setAnalysisId(rs.getInt("analysis_id"));
		String content =Jsoup.parse(rs.getString("content")).text();
		System.out.println("The Extracted Text content is :"+content);
		analysis.setContent(content);
		String respJsonStr=SentimentClient.getSentiment(content);
		if(respJsonStr!=null){
			JSONObject jsonObject=JSONObject.fromObject(respJsonStr);
			if(jsonObject.getBoolean("status")){
				analysis.setStatus(1);
				JSONObject jsonDataObj=jsonObject.getJSONObject("data");
				analysis.setScore(Float.parseFloat(jsonDataObj.getString("score")));
				analysis.setMagnitude(Float.parseFloat(jsonDataObj.getString("magnitude")));
				if(analysis.getScore()>0){
					analysis.setAnalysisResult(1);	
				}else if(analysis.getScore()<0){
					analysis.setAnalysisResult(-1);	
				}else{
					analysis.setAnalysisResult(0);	
				}

			}else{
				analysis.setStatus(-1);
			}
		}else{
			analysis.setStatus(0);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return analysis;
	}

}
