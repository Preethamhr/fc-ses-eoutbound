package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class CsatRequestsRowMapper implements RowMapper<CsatRequests>{

	@Override
	public CsatRequests mapRow(ResultSet rs, int rowNum) throws SQLException {
		//get the values from the result set
		Integer csatRequestId = rs.getInt("csat_request_id");
		Integer issueId=rs.getInt("issue_id");
		Integer companyId=rs.getInt("company_id");
		String csatContent = rs.getString("csat_content");
		String csatFrom = rs.getString("csat_from");
		String csatTo = rs.getString("csat_to");
		String csatSubject = rs.getString("csat_subject");
		
		
		CsatRequests csatRequests = new CsatRequests();
		csatRequests.setCsatRequestId(csatRequestId);
		csatRequests.setIssueId(issueId);
		csatRequests.setCompanyId(companyId);
		csatRequests.setCsatContent(csatContent);
		csatRequests.setCsatFrom(csatFrom);
		csatRequests.setCsatTo(csatTo);
		csatRequests.setCsatSubject(csatSubject);
		
		return csatRequests;
	}

}
