package com.wolken.selfservice.jdbc.handler.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wolken.selfservice.jdbc.connection.MailStatusProcessorUpdate;
import com.wolken.selfservice.jdbc.handler.EmailRequest;
import com.wolken.selfservice.jdbc.handler.vo.KbApprovalEmails;

public class KbApprovalEmailsRowMapper implements RowMapper<KbApprovalEmails>{

	@Override
	public KbApprovalEmails mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		System.out.println("starting looping row processor kb");
		KbApprovalEmails kbApprovalEmails = new KbApprovalEmails();
		try {
			kbApprovalEmails.setApprovalEmailId((Integer) rs.getInt("approval_email_id"));
			kbApprovalEmails.setArticleId((Integer) rs.getInt("article_id"));
			
			String htmlContent=(String)rs.getString("email_content");
			if(htmlContent!=null){
				htmlContent = htmlContent.trim().replaceAll("(\r\n|\n)", "<br />");
			}
			kbApprovalEmails.setEmailContent(htmlContent);

			String email_to = (String) rs.getString("email_to");
			
			
			String emails ="";
			if(email_to!=null && email_to.trim().length()>0){
				if(email_to.contains(";")){
					email_to=email_to.replaceAll(";", ",");
				}
				String email_list [] = email_to.split(",");
				for(String emailStr : email_list){
					if(emails.equals("")){
//						emails= EncryptionUtil.decryptString(emailStr);
						emails= emailStr;
					}else{
//						emails= emails +","+EncryptionUtil.decryptString(emailStr);
						emails= emails +","+emailStr;
					}
					
				}
				
				kbApprovalEmails.setEmailTo(emails);
			}
			kbApprovalEmails.setEmailFrom((String) rs.getString("email_from"));
			kbApprovalEmails.setEmailCC((String) rs.getString("email_cc"));
			
			
			if(rs.getString("email_subject") != null){
				String subject=(String) rs.getString("email_subject");
				subject=subject.replace("\n", " ");
				subject=subject.replace("\r", " ");
				kbApprovalEmails.setEmailSubject(subject);
			}
			
			if(kbApprovalEmails.getEmailCC()!=null && kbApprovalEmails.getEmailCC().contains(";")){
				String cc=kbApprovalEmails.getEmailCC();
				cc=cc.replaceAll(";", ",");
				kbApprovalEmails.setEmailCC(cc);
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println("ending looping row processor kb-->"+kbApprovalEmails.getEmailTo());
		return kbApprovalEmails;
	}

}
