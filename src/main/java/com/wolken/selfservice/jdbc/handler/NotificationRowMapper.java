package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class NotificationRowMapper implements RowMapper<Notification>{

	@Override
	public Notification mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		//get the values from the result set
		Integer id = rs.getInt("id");
		Integer status = rs.getInt("status");
		Integer sourceId = rs.getInt("source_id");
		Integer eventId = rs.getInt("event_id");
		Integer companyId = rs.getInt("company_id");
		String emailTo = rs.getString("email_to");
		String emaliCc = rs.getString("emali_cc");
		String content = rs.getString("content");
		Integer issueId = rs.getInt("issue_id");
		String notificationType = rs.getString("notification_type");
		Integer createdBy = rs.getInt("created_by");
		Integer updatedBy = rs.getInt("created_date");
		String createdDate = rs.getString("updated_by");
		String updatedDate = rs.getString("updated_date"); 
		String subject = rs.getString("subject");
		
		
		//Create an notification object and return
		Notification notification = new Notification();
		notification.setId(id);
		notification.setStatus(status);
		notification.setSourceId(sourceId);
		notification.setEventId(eventId);
		notification.setCompanyId(companyId);
		notification.setEmailTo(emailTo);
		notification.setEmaliCc(emaliCc);
		notification.setContent(content);
		notification.setIssueId(issueId);
		notification.setNotificationType(notificationType);
		notification.setCreatedBy(createdBy);
		notification.setUpdatedBy(updatedBy);
		notification.setCreatedDate(createdDate);
		notification.setUpdatedDate(updatedDate);
		notification.setSubject(subject);
		
		return notification;
	}

}
