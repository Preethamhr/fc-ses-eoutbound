package com.wolken.selfservice.jdbc.handler;

import java.util.Date;

public class TwitterCredentials {
	
	public Integer twitterCredentialsId;
	public Integer twitterAccountPkey;
	public Integer companyId;
	public Long twitterAccountId;
	public String consumerKey;
	public String consumerSecret;
	public String accountToken;
	public String accountTokenSecret;
	public String pageName;
	public String pageScreenName;
	public String pagePicURL;
	public String streamingStatus;
	public Integer createdBy;
	public Date createdDate;
	
	
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getTwitterCredentialsId() {
		return twitterCredentialsId;
	}
	public void setTwitterCredentialsId(Integer twitterCredentialsId) {
		this.twitterCredentialsId = twitterCredentialsId;
	}
	public Integer getTwitterAccountPkey() {
		return twitterAccountPkey;
	}
	public void setTwitterAccountPkey(Integer twitterAccountPkey) {
		this.twitterAccountPkey = twitterAccountPkey;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Long getTwitterAccountId() {
		return twitterAccountId;
	}
	public void setTwitterAccountId(Long twitterAccountId) {
		this.twitterAccountId = twitterAccountId;
	}
	public String getConsumerKey() {
		return consumerKey;
	}
	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}
	public String getConsumerSecret() {
		return consumerSecret;
	}
	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}
	public String getAccountToken() {
		return accountToken;
	}
	public void setAccountToken(String accountToken) {
		this.accountToken = accountToken;
	}
	public String getAccountTokenSecret() {
		return accountTokenSecret;
	}
	public void setAccountTokenSecret(String accountTokenSecret) {
		this.accountTokenSecret = accountTokenSecret;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageScreenName() {
		return pageScreenName;
	}
	public void setPageScreenName(String pageScreenName) {
		this.pageScreenName = pageScreenName;
	}
	public String getPagePicURL() {
		return pagePicURL;
	}
	public void setPagePicURL(String pagePicURL) {
		this.pagePicURL = pagePicURL;
	}
	public String getStreamingStatus() {
		return streamingStatus;
	}
	public void setStreamingStatus(String streamingStatus) {
		this.streamingStatus = streamingStatus;
	}
	
	

}
