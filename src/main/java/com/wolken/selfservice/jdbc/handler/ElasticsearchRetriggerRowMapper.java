package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ElasticsearchRetriggerRowMapper implements RowMapper<ElasticsearchRetrigger>{
	
	
	


	@Override
	public ElasticsearchRetrigger mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		ElasticsearchRetrigger issueDetailsForretrigger= new ElasticsearchRetrigger();
		try{
			
			issueDetailsForretrigger.setCompanyId(rs.getInt("company_id"));
			issueDetailsForretrigger.setId(rs.getInt("id"));
			issueDetailsForretrigger.setTicketId(rs.getInt("ticket_id"));
			issueDetailsForretrigger.setTicketDetails(rs.getString("ticket_details"));
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return issueDetailsForretrigger;
	}



}
