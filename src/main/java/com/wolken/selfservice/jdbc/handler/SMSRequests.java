package com.wolken.selfservice.jdbc.handler;

import java.util.Date;

public class SMSRequests {
	

	private Integer smsRequestId;
	private Integer companyId;
	private String smsRequestType;
	private String smsRequestInfo;
	private String smsRequestStatus;
	private String smsTo;
	private String smsFrom;
	private Date creationDate;
	private Integer userId;
	private String errorStacktrace;
	private Integer issueId;
	
	
	public Integer getSmsRequestId() {
		return smsRequestId;
	}
	public void setSmsRequestId(Integer smsRequestId) {
		this.smsRequestId = smsRequestId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getSmsRequestType() {
		return smsRequestType;
	}
	public void setSmsRequestType(String smsRequestType) {
		this.smsRequestType = smsRequestType;
	}
	public String getSmsRequestInfo() {
		return smsRequestInfo;
	}
	public void setSmsRequestInfo(String smsRequestInfo) {
		this.smsRequestInfo = smsRequestInfo;
	}
	public String getSmsRequestStatus() {
		return smsRequestStatus;
	}
	public void setSmsRequestStatus(String smsRequestStatus) {
		this.smsRequestStatus = smsRequestStatus;
	}
	public String getSmsTo() {
		return smsTo;
	}
	public void setSmsTo(String smsTo) {
		this.smsTo = smsTo;
	}
	public String getSmsFrom() {
		return smsFrom;
	}
	public void setSmsFrom(String smsFrom) {
		this.smsFrom = smsFrom;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getErrorStacktrace() {
		return errorStacktrace;
	}
	public void setErrorStacktrace(String errorStacktrace) {
		this.errorStacktrace = errorStacktrace;
	}
	public Integer getIssueId() {
		return issueId;
	}
	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}



}
