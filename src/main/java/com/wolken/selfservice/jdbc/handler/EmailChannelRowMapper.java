package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;



public class EmailChannelRowMapper implements RowMapper<EmailChannel>{

	@Override
	public EmailChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		
		EmailChannel emailChannel = new EmailChannel();
		emailChannel.setChannelEmailDetailsId(rs.getInt("channel_email_details_id"));
		emailChannel.setCompanyId(rs.getInt("company_id"));
		emailChannel.setCreatedBy(rs.getInt("created_by"));
		emailChannel.setCreatedDate(rs.getString("created_date"));
		emailChannel.setForwardingSupportEmailAddress(rs.getString("forwarding_support_email_address"));
		emailChannel.setForwardingSupportEmailPassword(rs.getString("forwarding_support_email_password"));
		emailChannel.setIssueSourceId(rs.getInt("issue_source_id"));
		emailChannel.setSupportEmailAddress(rs.getString("support_email_address"));
		emailChannel.setSupportEmailIsActive(rs.getBoolean("support_email_is_active"));
		emailChannel.setSupportEmailIsChanged(rs.getBoolean("support_email_is_changed"));
		emailChannel.setSupportEmailName(rs.getString("support_email_name"));
		emailChannel.setUpdatedBy(rs.getInt("updated_by"));
		emailChannel.setUpdatedDate(rs.getString("updated_date"));
		
		return emailChannel;
		
	}

}
