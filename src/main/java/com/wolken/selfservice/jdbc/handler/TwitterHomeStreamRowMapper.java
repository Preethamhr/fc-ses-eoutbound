package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class TwitterHomeStreamRowMapper implements RowMapper<TwitterHomeAccountStreaming>{
	@Override
	public TwitterHomeAccountStreaming mapRow(ResultSet rs, int rowNum) throws SQLException {
		Integer twitterHomeAccountStreamingId = rs.getInt("twitter_home_account_streaming_id");
		Integer twitterStreamingPkey = rs.getInt("twitter_streaming_pkey");
		Integer companyId = rs.getInt("company_id");
		Short twitterStreamingStatus = rs.getShort("twitter_streaming_status");
		
		TwitterHomeAccountStreaming accountStreaming = new TwitterHomeAccountStreaming();
		accountStreaming.setTwitterHomeAccountStreamingId(twitterHomeAccountStreamingId);
		accountStreaming.setTwitterStreamingPkey(twitterStreamingPkey);
		accountStreaming.setTwitterStreamingStatus(twitterStreamingStatus);
		accountStreaming.setCompanyId(companyId);

		return accountStreaming;
	}
}
