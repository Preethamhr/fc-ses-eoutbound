package com.wolken.selfservice.jdbc.handler;

//import java.io.FileNotFoundException;
import java.util.List;

//import javax.mail.internet.AddressException;

import com.wolken.selfservice.sendgrid.EmailViaSendGrid;

public class NotificationMessageHandler {
	
	
	
	public void handleMessage(List<Notification> notificationList) {
        
        for(Notification notification : notificationList){
        	
        	//String attachmentsPath = null;
			String to = notification.getEmailTo();
			String from = "no-reply@wolkencare.com";
			String subject = notification.getSubject();
			String body = notification.getContent();
			EmailViaSendGrid.sendMailFromSendGridNotification(to, from, subject, body);
        	
        }
    }

}
