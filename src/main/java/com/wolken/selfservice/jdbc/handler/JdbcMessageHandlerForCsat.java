package com.wolken.selfservice.jdbc.handler;

import java.util.List;

import com.wolken.selfservice.sendgrid.EmailViaSendGrid;

public class JdbcMessageHandlerForCsat {
	
	
	public void handleMessage(List<CsatRequests> csatRequestsList) {
		for(CsatRequests csatRequests:csatRequestsList){
			String to = csatRequests.getCsatTo();
			String from = csatRequests.getCsatFrom();
			String subject = csatRequests.getCsatSubject();
			String body = csatRequests.getCsatContent();
			EmailViaSendGrid.sendMailFromSendGridNotification(to, from, subject, body);
			}
		
		}

}
