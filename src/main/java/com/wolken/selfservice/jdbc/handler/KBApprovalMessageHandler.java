/**
 * 
 */
package com.wolken.selfservice.jdbc.handler;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.wolken.selfservice.jdbc.connection.MailStatusProcessorUpdate;
import com.wolken.selfservice.jdbc.handler.vo.KbApprovalEmails;
import com.wolken.selfservice.sendgrid.EmailViaSendGrid;

/**
 * @author Raghavendra
 *
 */
public class KBApprovalMessageHandler {

	
	
	public void handleMessage(List<KbApprovalEmails> kbApprovalEmailsList) throws AddressException, FileNotFoundException, MessagingException, URISyntaxException {
		System.out.println("Inside handleMessage ");
		try{
			MailInfo mailInfo = null;		
			MailStatusProcessorUpdate wipProcessor=null;
			for(KbApprovalEmails kbApprovalEmails : kbApprovalEmailsList){
				wipProcessor=new MailStatusProcessorUpdate();
				mailInfo = new MailInfo();
				
				
				mailInfo.setEmail_to(kbApprovalEmails.getEmailTo());
				mailInfo.setEmail_cc(kbApprovalEmails.getEmailCC());
				mailInfo.setEmail_from(kbApprovalEmails.getEmailFrom());
				mailInfo.setSubject(kbApprovalEmails.getEmailSubject());
				mailInfo.setEmail_request_info(kbApprovalEmails.getEmailContent());
				
				try{
					EmailViaSendGrid.sendMailFromSES(mailInfo);
					wipProcessor.updateMailStatusWithTable("elasticsearch.kb_approval_emails","status","approval_email_id",kbApprovalEmails.getApprovalEmailId(),2);
				}catch(Exception e){
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					e.printStackTrace();
					
					wipProcessor.updateMailStatusWithErrorWithTable("elasticsearch.kb_approval_emails","status","approval_email_id",kbApprovalEmails.getApprovalEmailId(),3,e.toString() + sw.toString());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
