package com.wolken.selfservice.jdbc.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.google.common.base.Joiner;
import com.wolken.selfservice.util.RestUtil;


public class IssueTimelyActionsHandler {
	
	//@Value("${reOpenUrl}")
	private String reOpenUrl;
	public void handleMessage(List<IssueTimelyActions> issueTimelyActions) {
		List<Integer> tickets=null;
		if(issueTimelyActions!=null&& issueTimelyActions.size()>0){
			System.out.println("in IssueTimelyActionsHandler "+issueTimelyActions.size()+issueTimelyActions.toString());	
			tickets= new ArrayList<Integer>();
			for(IssueTimelyActions action:issueTimelyActions){
				tickets.add(action.getIssueId());
			}
		}
		
		String ticketIds=Joiner.on(',').join(tickets);
		//String ticketIds=tickets.toString().replace("[", "").replace("]", "");
		System.out.println(" *** Calling pendingtoreopenrule *** URL : "+reOpenUrl+"&tickets="+ticketIds);
		RestUtil.httpGetCall(reOpenUrl+"&tickets="+ticketIds);
		System.out.println(" *** Ending  pendingtoreopenrule ***");
	}

}
