package com.wolken.selfservice.jdbc.handler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import com.wolken.selfservice.service.GenerateXML;
import com.wolken.selfservice.service.impl.GenerateXMLImpl;

public class JdbcMessageHandler {
	
	public void handleMessage(List<EmailChannel> emailChannelList){
		
		try {
		GenerateXML generateXML = new GenerateXMLImpl();
		generateXML.generateXML();
		Thread.sleep(5000);
		
		deployMailIntegration();
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	
	public static void deployMailIntegration(){
		 Runtime r = Runtime.getRuntime();
		 try {
	        Process p = null;
	        
	        String[] cmd = {"sh",  "/issuetracker/config/self-deploy.sh"};
	        
	            p = r.exec(cmd);
	            
	            BufferedReader stdInput = new BufferedReader(new
	                    InputStreamReader(p.getInputStream()));
	            String s;                                                                
	            while ((s = stdInput.readLine()) != null) {                                
	              System.out.println("Upload status output: " + s);                             
	            }   
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}

}
