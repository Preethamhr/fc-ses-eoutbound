package com.wolken.selfservice.jdbc.handler.vo;



public class KbApprovalEmails {

	
	private int approvalEmailId;
	
	
	
	private long articleId;
	
	private String emailFrom;
	
	private String emailTo;
	
	private String emailContent;
	
	private int status;
	
	private long createdTime;
	


	
	private String emailSubject;
	
	private String emailCC;
	
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	public int getApprovalEmailId() {
		return approvalEmailId;
	}
	public void setApprovalEmailId(int approvalEmailId) {
		this.approvalEmailId = approvalEmailId;
	}
	public long getArticleId() {
		return articleId;
	}
	public void setArticleId(long articleId) {
		this.articleId = articleId;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getEmailContent() {
		return emailContent;
	}
	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}
	public String getEmailCC() {
		return emailCC;
	}
	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}
	@Override
	public String toString() {
		return "KbApprovalEmails [approvalEmailId=" + approvalEmailId + ", articleId=" + articleId + ", emailFrom="
				+ emailFrom + ", emailTo=" + emailTo + ", createdTime=" + createdTime + ", approvalTypeFlag="
				+ "]";
	}
	
	
	
	
}
