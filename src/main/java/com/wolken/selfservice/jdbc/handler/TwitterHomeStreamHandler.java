package com.wolken.selfservice.jdbc.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.wolken.selfservice.util.RestUtil;

public class TwitterHomeStreamHandler {

	//@Value("${homeStreamTktCreationUrl}")
	private String homeStreamTktCreationUrl;

	public void handleMessage(List<TwitterHomeAccountStreaming> TwitterHomeAccountStreamingLst){
		StringBuilder builder = new StringBuilder();
		for(TwitterHomeAccountStreaming homeAccountStreaming : TwitterHomeAccountStreamingLst){
			builder.setLength(0);
			System.out.println("homeAccountStreaming:: "+homeAccountStreaming.getTwitterHomeAccountStreamingId());
			System.out.println("company:: "+homeAccountStreaming.getCompanyId());
			builder.append(RestUtil.httpGetCall(homeStreamTktCreationUrl+"?streamingid="+
			homeAccountStreaming.getTwitterStreamingPkey()+"&companyid="+homeAccountStreaming.getCompanyId()));
		}
	}

}
