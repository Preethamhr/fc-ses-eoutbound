package com.wolken.selfservice.jdbc.handler;

public class EmailChannel {
	
	private Integer channelEmailDetailsId;
	private String supportEmailName;
	private String supportEmailAddress;
	private String forwardingSupportEmailAddress;
	private String forwardingSupportEmailPassword;	
	private boolean supportEmailIsActive;
	private boolean supportEmailIsChanged;
	private Integer issueSourceId;
	private Integer companyId;
	private Integer createdBy;	
	private String createdDate;
	private Integer updatedBy;
	private String updatedDate;
	
	
	public Integer getChannelEmailDetailsId() {
		return channelEmailDetailsId;
	}
	public void setChannelEmailDetailsId(Integer channelEmailDetailsId) {
		this.channelEmailDetailsId = channelEmailDetailsId;
	}
	public String getSupportEmailName() {
		return supportEmailName;
	}
	public void setSupportEmailName(String supportEmailName) {
		this.supportEmailName = supportEmailName;
	}
	public String getSupportEmailAddress() {
		return supportEmailAddress;
	}
	public void setSupportEmailAddress(String supportEmailAddress) {
		this.supportEmailAddress = supportEmailAddress;
	}
	public String getForwardingSupportEmailAddress() {
		return forwardingSupportEmailAddress;
	}
	public void setForwardingSupportEmailAddress(
			String forwardingSupportEmailAddress) {
		this.forwardingSupportEmailAddress = forwardingSupportEmailAddress;
	}
	public String getForwardingSupportEmailPassword() {
		return forwardingSupportEmailPassword;
	}
	public void setForwardingSupportEmailPassword(
			String forwardingSupportEmailPassword) {
		this.forwardingSupportEmailPassword = forwardingSupportEmailPassword;
	}
	public boolean isSupportEmailIsActive() {
		return supportEmailIsActive;
	}
	public void setSupportEmailIsActive(boolean supportEmailIsActive) {
		this.supportEmailIsActive = supportEmailIsActive;
	}
	public boolean isSupportEmailIsChanged() {
		return supportEmailIsChanged;
	}
	public void setSupportEmailIsChanged(boolean supportEmailIsChanged) {
		this.supportEmailIsChanged = supportEmailIsChanged;
	}
	public Integer getIssueSourceId() {
		return issueSourceId;
	}
	public void setIssueSourceId(Integer issueSourceId) {
		this.issueSourceId = issueSourceId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	

}
