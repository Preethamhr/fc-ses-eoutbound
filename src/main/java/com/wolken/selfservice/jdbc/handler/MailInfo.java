package com.wolken.selfservice.jdbc.handler;


public class MailInfo {

	private String email_request_info; 
	private String checkCompanyId;
	
	private String email_to;
	private String email_from;
	private String email_cc;
	private String email_bcc;
	private String attachPath;
	private String subject;
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "MailInfo [email_request_info=" + email_request_info + ", checkCompanyId=" + checkCompanyId
				+ ", email_to=" + email_to + ", email_from=" + email_from + ", email_cc=" + email_cc + ", email_bcc="
				+ email_bcc + ", attachPath=" + attachPath + ", subject=" + subject + "]";
	}

	public String getEmail_request_info() {
		return email_request_info;
	}

	public void setEmail_request_info(String email_request_info) {
		this.email_request_info = email_request_info;
	}

	public String getCheckCompanyId() {
		return checkCompanyId;
	}

	public void setCheckCompanyId(String checkCompanyId) {
		this.checkCompanyId = checkCompanyId;
	}

	public String getEmail_to() {
		return email_to;
	}

	public void setEmail_to(String email_to) {
		this.email_to = email_to;
	}

	public String getEmail_from() {
		return email_from;
	}

	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}

	public String getEmail_cc() {
		return email_cc;
	}

	public void setEmail_cc(String email_cc) {
		this.email_cc = email_cc;
	}

	public String getEmail_bcc() {
		return email_bcc;
	}

	public void setEmail_bcc(String email_bcc) {
		this.email_bcc = email_bcc;
	}

	public String getAttachPath() {
		return attachPath;
	}

	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}
	
}
