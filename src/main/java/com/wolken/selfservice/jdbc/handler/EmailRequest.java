package com.wolken.selfservice.jdbc.handler;

import java.util.Date;

public class EmailRequest {
	
	
	
	
	private Integer emailRequestId;
	private Integer companyId;	
	private Integer emailRequestTypeId;
	private String emailRequestTypeName;
	private String emailRequestInfo;
	private Integer emailRequestStatus;
	private String emailTo;
	private String emailFrom;
	private String emailCC;
	private String emailBcc;
	private long creationDate;	
	private Integer userId;	
	private String errorStacktrace;
	private String emailSubject;
	private Boolean active;
	private Integer responseTypeId;
	private String emailAttachments;
	private Integer rulesEngineId;
	private String emailType;
	
	private Integer errorRetry;
	
	public Integer getEmailRequestStatus() {
		return emailRequestStatus;
	}
	public void setEmailRequestStatus(Integer emailRequestStatus) {
		this.emailRequestStatus = emailRequestStatus;
	}
	public String getEmailRequestTypeName() {
		return emailRequestTypeName;
	}
	public void setEmailRequestTypeName(String emailRequestTypeName) {
		this.emailRequestTypeName = emailRequestTypeName;
	}
	public String getEmailType() {
		return emailType;
	}
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	public Integer getEmailRequestId() {
		return emailRequestId;
	}
	public void setEmailRequestId(Integer emailRequestId) {
		this.emailRequestId = emailRequestId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public Integer getEmailRequestTypeId() {
		return emailRequestTypeId;
	}
	public void setEmailRequestTypeId(Integer emailRequestTypeId) {
		this.emailRequestTypeId = emailRequestTypeId;
	}
	public String getEmailRequestInfo() {
		return emailRequestInfo;
	}
	public void setEmailRequestInfo(String emailRequestInfo) {
		this.emailRequestInfo = emailRequestInfo;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getEmailFrom() {
		return emailFrom;
	}
	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}
	public String getEmailCC() {
		return emailCC;
	}
	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}
	public String getEmailBcc() {
		return emailBcc;
	}
	public void setEmailBcc(String emailBcc) {
		this.emailBcc = emailBcc;
	}
	public long getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getErrorStacktrace() {
		return errorStacktrace;
	}
	public void setErrorStacktrace(String errorStacktrace) {
		this.errorStacktrace = errorStacktrace;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Integer getResponseTypeId() {
		return responseTypeId;
	}
	public void setResponseTypeId(Integer responseTypeId) {
		this.responseTypeId = responseTypeId;
	}
	public String getEmailAttachments() {
		return emailAttachments;
	}
	public void setEmailAttachments(String emailAttachments) {
		this.emailAttachments = emailAttachments;
	}
	public Integer getRulesEngineId() {
		return rulesEngineId;
	}
	public void setRulesEngineId(Integer rulesEngineId) {
		this.rulesEngineId = rulesEngineId;
	}
	public Integer getErrorRetry() {
		return errorRetry;
	}
	public void setErrorRetry(Integer errorRetry) {
		this.errorRetry = errorRetry;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	

}
