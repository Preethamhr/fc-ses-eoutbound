package com.wolken.selfservice.jdbc.handler;

public class TwitterHomeAccountStreaming {

	private Integer twitterHomeAccountStreamingId;
	private Integer twitterStreamingPkey;
	private short twitterStreamingStatus;
	private Integer companyId;
	
	public Integer getTwitterHomeAccountStreamingId() {
		return twitterHomeAccountStreamingId;
	}
	public void setTwitterHomeAccountStreamingId(Integer twitterHomeAccountStreamingId) {
		this.twitterHomeAccountStreamingId = twitterHomeAccountStreamingId;
	}
	public Integer getTwitterStreamingPkey() {
		return twitterStreamingPkey;
	}
	public void setTwitterStreamingPkey(Integer twitterStreamingPkey) {
		this.twitterStreamingPkey = twitterStreamingPkey;
	}
	public short getTwitterStreamingStatus() {
		return twitterStreamingStatus;
	}
	public void setTwitterStreamingStatus(short twitterStreamingStatus) {
		this.twitterStreamingStatus = twitterStreamingStatus;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	
	
}
