package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

public class TwitterAccountRowMapper implements RowMapper<TwitterCredentials>{

	@Override
	public TwitterCredentials mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
				//get the values from the result set
				Integer twitter_credentials_id = rs.getInt("twitter_credentials_id");
				Integer twitter_account_pkey = rs.getInt("twitter_account_pkey");
				Long twitter_acount_id = rs.getLong("twitter_acount_id");
				Integer companyId = rs.getInt("company_id");
				String consumer_key = rs.getString("consumer_key");
				String consumer_secret = rs.getString("consumer_secret");
				String access_token = rs.getString("access_token");
				String access_token_secret = rs.getString("access_token_secret");
				String page_name = rs.getString("page_name");
				String page_screen_name = rs.getString("page_screen_name"); 
				String page_pic_url = rs.getString("page_pic_url");
				String streaming_status = rs.getString("streaming_status");
				Integer created_by = rs.getInt("created_by");
				Date created_date = rs.getDate("created_date");
				
				
				TwitterCredentials twitterCredentials = new TwitterCredentials();
				twitterCredentials.setAccountToken(access_token);
				twitterCredentials.setAccountTokenSecret(access_token_secret);
				twitterCredentials.setCompanyId(companyId);
				twitterCredentials.setConsumerKey(consumer_key);
				twitterCredentials.setConsumerSecret(consumer_secret);
				twitterCredentials.setCreatedBy(created_by);
				twitterCredentials.setCreatedDate(created_date);
				twitterCredentials.setPageName(page_name);
				twitterCredentials.setPagePicURL(page_pic_url);
				twitterCredentials.setPageScreenName(page_screen_name);
				twitterCredentials.setStreamingStatus(streaming_status);
				twitterCredentials.setTwitterAccountId(twitter_acount_id);
				twitterCredentials.setTwitterAccountPkey(twitter_account_pkey);
				twitterCredentials.setTwitterCredentialsId(twitter_credentials_id);
		
				
				
				return twitterCredentials;
	}

}
