package com.wolken.selfservice.jdbc.handler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.wolken.selfservice.util.ConfigurationPropertiesUtil;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class SentimentClient {

	static Client client = Client.create();
	static String urlStatic=ConfigurationPropertiesUtil.sentimentAnalysisURL;
	
	public static String  getSentiment(String content){
		ClientResponse response = null;
		JSONObject respJsonObject= null;
		try {
			System.out.println("Inside getSentiment mtd");
			String url="";
			try {
				url = urlStatic.concat(URLEncoder.encode(content,"UTF-8")).replace(" ", "%20");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			client.getProperties().put(ClientConfig.PROPERTY_CONNECT_TIMEOUT, 1000);
			client.getProperties().put(ClientConfig.PROPERTY_READ_TIMEOUT, 10000);
			
			
			WebResource webResource = client.resource(url);
			JSONObject jsonObject= new JSONObject(); 
			jsonObject.put("text",content);
			response=webResource.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11").accept("application/json").get(ClientResponse.class);
			if(response.getStatus()==200){
				System.out.println("Successfully received sentiment API resp");
				String resp=response.getEntity(String.class);
				respJsonObject=JSONObject.fromObject(resp);
			}else{
				respJsonObject= new JSONObject();
				respJsonObject.put("status", false);
			}
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respJsonObject.toString();
	}
}
