package com.wolken.selfservice.jdbc.handler;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class IssueTimelyActionsRowMapper implements RowMapper<IssueTimelyActions>{

	@Override
	public IssueTimelyActions mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		IssueTimelyActions actions= new IssueTimelyActions();
		try{
			actions.setId(rs.getInt("id"));
			actions.setIssueId(rs.getInt("issue_id"));
		}catch(Exception e){
			e.printStackTrace();
		}
		return actions;
	}

}
