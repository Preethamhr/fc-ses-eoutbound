package com.wolken.selfservice.jdbc.handler;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.List;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.wolken.selfservice.jdbc.connection.MailStatusProcessorUpdate;
import com.wolken.selfservice.sendgrid.EmailViaSendGrid;

public class EmailRequestMessageHandler {


	/*
	 * Commenting Sendgrid Usage
	 */

	/*public void handleMessage(List<EmailRequest> emailRequestList) {


		 //AmazonSESServiceImpl emailService = new AmazonSESServiceImpl();
		//EmailViaSendGrid sendGrid = new EmailViaSendGrid();

	        for(EmailRequest emailRequest : emailRequestList){

	        	//Message message = createNotificationMessage(emailRequest);
				//emailService.transportMessage(message);
				String attachmentsPath = emailRequest.getDoc_full_path();
				String to = emailRequest.getEmail_to();
				String bcc = emailRequest.getEmail_bcc();
				String cc = emailRequest.getEmail_cc();
				String from = emailRequest.getEmail_from();
				String subject = "Ticket # "+emailRequest.getIssue_id()+ " - "+emailRequest.getEmail_subject();
				String body = emailRequest.getEmail_request_info();
				//EmailViaSendGrid.sendMailFromSendGrid(to, from, subject, body, attachmentsPath);

				//from name sending null for now as we dont have a fiend in email request table.
				EmailViaSendGrid.sendMailFromSendGrid(to,cc,bcc,from,null,subject, body, attachmentsPath);



	        }
	}*/
	
	//Using AmazonSES to poll email
	
		public void handleMessage(List<EmailRequest> emailRequestList) throws AddressException, FileNotFoundException, MessagingException, URISyntaxException {
			System.out.println("Inside handleMessage ");
			try{
				MailInfo mailInfo = null;		
				MailStatusProcessorUpdate wipProcessor=null;
				for(EmailRequest emailRequest : emailRequestList){
					wipProcessor=new MailStatusProcessorUpdate();
					mailInfo = new MailInfo();
					
					
					mailInfo.setEmail_to(emailRequest.getEmailTo());
					mailInfo.setEmail_bcc(emailRequest.getEmailBcc());
					mailInfo.setEmail_cc(emailRequest.getEmailCC());
					mailInfo.setEmail_from(emailRequest.getEmailFrom());
					mailInfo.setSubject(emailRequest.getEmailSubject());
					mailInfo.setEmail_request_info(emailRequest.getEmailRequestInfo());
					mailInfo.setAttachPath(emailRequest.getEmailAttachments());
					
					try{
						EmailViaSendGrid.sendMailFromSES(mailInfo);
						wipProcessor.updateMailStatus(emailRequest.getEmailRequestId(),"SUCCESS");
					}catch(Exception e){
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						e.printStackTrace();
						Integer errorRetry=emailRequest.getErrorRetry();
						wipProcessor.updateMailStatusWithError(emailRequest.getEmailRequestId(),"ERROR",e.toString() + sw.toString(),errorRetry==null ? 1 :++errorRetry);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}



	public Message createNotificationMessage(EmailRequest emailRequest) throws MessagingException,
	AddressException, FileNotFoundException {
		System.out.println("Inside createNotificationMessage ");
		Message message = null;
		Session session = null;
		try {
			// Create a new Message
			message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailRequest.getEmailFrom()));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(emailRequest.getEmailTo()));
			if (emailRequest.getEmailCC() != null
					&& !emailRequest.getEmailCC().isEmpty()) {
				message.setRecipients(Message.RecipientType.CC,
						InternetAddress.parse(emailRequest.getEmailCC()));
			}
			Address[] addresses = new Address[1];
			addresses[0] = new InternetAddress(emailRequest.getEmailFrom());

			message.setReplyTo(addresses);

			//message.setSubject("Notification - Ticket # "+notification.getIssueId());
			message.setSubject("Ticket # "+emailRequest+ " - "+emailRequest.getEmailSubject());

			Multipart multipart = new MimeMultipart();
			MimeBodyPart messagePart = new MimeBodyPart();
			messagePart.setContent(emailRequest.getEmailRequestInfo(), "text/html");
			multipart.addBodyPart(messagePart);
			message.setContent(multipart);




		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
}
