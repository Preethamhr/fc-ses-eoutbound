/**
 * 
 */
package com.wolken.selfservice.jdbc.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Raghavendra
 *
 */
public class MailStatusProcessorUpdate {
	
	
	public void updateMailStatus(Integer emailRequestId,String status){
		System.out.println("Update started email_request_id-->"+emailRequestId +" with status ->"+status);
		PreparedStatement pstmt=null;
		try{
			if(emailRequestId!=null){
				JdbcConnection jdbc=new JdbcConnection();
				Connection con=jdbc.getConnection();
				String sqlUpdate = "UPDATE email_requests "
	                + " SET email_request_status= ? "
	                + " WHERE email_request_id= ?";

				pstmt = con.prepareStatement(sqlUpdate);
				pstmt.setString(1, status);
				pstmt.setInt(2, emailRequestId);
				int rowAffected = pstmt.executeUpdate();
	            System.out.println(String.format("Row affected %d", rowAffected));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Update end email_request_id-->"+emailRequestId +" with status ->"+status);
	}
	
	public void updateMailStatusWithError(Integer emailRequestId,String status,String error,Integer errorRetry){
		System.out.println("Update started email_request_id-->"+emailRequestId +" with status ->"+status);
		PreparedStatement pstmt=null;
		try{
			if(emailRequestId!=null){
				JdbcConnection jdbc=new JdbcConnection();
				Connection con=jdbc.getConnection();
				String sqlUpdate = "UPDATE email_requests "
	                + " SET email_request_status_id= ?,error_stacktrace=? , error_retry=?"
	                + " WHERE email_request_id= ?";

				pstmt = con.prepareStatement(sqlUpdate);
				pstmt.setString(1, status);
				pstmt.setString(2, error==null? "error stacktrace not generated":error.substring(0, 4000));
				pstmt.setInt(3, errorRetry);
				pstmt.setInt(4, emailRequestId);
				int rowAffected = pstmt.executeUpdate();
	            System.out.println(String.format("Row affected %d", rowAffected));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Update end email_request_id-->"+emailRequestId +" with status ->"+status);
	}
	
	
	public void updateMailStatusWithTable(String tableName,String setColumn,String whereColumn,Integer id,int status){
		System.out.println("Update started "+tableName+"-->"+id +" with status ->"+status);
		PreparedStatement pstmt=null;
		try{
			if(id!=null){
				JdbcConnection jdbc=new JdbcConnection();
				Connection con=jdbc.getConnection();
				String sqlUpdate = "UPDATE  "+tableName
	                + " SET "+setColumn+" = ? "
	                + " WHERE "+whereColumn+" = ?";

				pstmt = con.prepareStatement(sqlUpdate);
				pstmt.setInt(1, status);
				pstmt.setInt(2, id);
				int rowAffected = pstmt.executeUpdate();
	            System.out.println(String.format("Row affected %d", rowAffected));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Update end "+tableName+"-->"+id +" with status ->"+status);
	}
	
	
	public void updateMailStatusWithErrorWithTable(String tableName,String setColumn,String whereColumn,Integer id,int status,String error){
		System.out.println("Update error started "+tableName+"-->"+id +" with status ->"+status);
		PreparedStatement pstmt=null;
		try{
			if(id!=null){
				JdbcConnection jdbc=new JdbcConnection();
				Connection con=jdbc.getConnection();
				String sqlUpdate = "UPDATE  "+tableName
	                + " SET "+setColumn+" = ?,error_stacktrace=? "
	                + " WHERE "+whereColumn+"= ?";

				pstmt = con.prepareStatement(sqlUpdate);
				pstmt.setInt(1, status);
				pstmt.setString(2, error==null? "error stacktrace not generated":error.substring(0, 4000));
				
				pstmt.setInt(3, id);
				int rowAffected = pstmt.executeUpdate();
	            System.out.println(String.format("Row affected %d", rowAffected));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(pstmt!=null){
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("Update error ended "+tableName+"-->"+id +" with status ->"+status);
	}
	

}
