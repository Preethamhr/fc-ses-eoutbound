/**
 * 
 */
package com.wolken.selfservice.jdbc.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


/**
 * @author Raghavendra
 *
 */
public class JdbcConnection {
	
	private static Connection conn=null;
	public Connection getConnection(){
		try{
			if(conn==null){
				conn=connect();
			}else{
				System.out.println("---return existing connection---");
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
		}	
		return conn;
	}
	
	public void closeConnection(){
		try{
			if(conn==null){
				conn.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e);
		}	
	}
	
	private Connection connect(){
		System.out.println("---connecting new connection---");
		Connection con=null;
		Properties prop=loadDBproperties();
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
			con=DriverManager.getConnection(prop.getProperty("database.connection_url"),
					prop.getProperty("database.user"),prop.getProperty("database.password"));  
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch(SQLException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("---new connection generated---");
		return con;
	}
	
	
	private static Properties properties = null;
	private Properties loadDBproperties(){
		InputStream input = null;
		try {
			if(properties==null){
				properties=new Properties();
				input = new FileInputStream(System.getenv("CONFIG_DIR") + "/configuration.properties");
				properties.load(input);
			}else{
				return  properties;
			}
		} catch (IOException e) {
			System.out.println("Property file does not exist");
		}catch(Exception e){
			e.printStackTrace();
		}
		return properties;
	}
	

}
