package com.wolken.selfservice.service.impl;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;









import com.wolken.selfservice.aws.AmazonS3ServiceImpl;
import com.wolken.selfservice.aws.StorageService;
import com.wolken.selfservice.model.ChannelEmailDetailsPerCompanyVO;
import com.wolken.selfservice.service.GenerateXML;

public class GenerateXMLImpl implements GenerateXML{


	private static final Logger LOGGER = Logger.getLogger(GenerateXML.class);

	private static final String MASTER_DB_CONFIGURATION="/issuetracker/config/configuration.properties";

	private static  String uploadType=null;

	public static Connection getConnection(){
		System.out.println("=====Inside getting connection=====");
		Properties prop = new Properties();
		InputStream input = null;
		Connection conn=null;

		try{

			input = new FileInputStream(System.getenv("CONFIG_DIR")+"/configuration.properties");
			prop.load(input);

			String userName=prop.getProperty("database.user");
			String password=prop.getProperty("database.password");
			String driver=prop.getProperty("database.driver");
			String connectionUrl=prop.getProperty("database.connection_url");
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection (connectionUrl, userName, password);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("=====got connection=====");
		return conn;
	}


	public static void configProperties(){
		System.out.println("=====Inside configProperties =====");
		Properties prop = new Properties();
		InputStream input = null;
		try{
			input = new FileInputStream(System.getenv("CONFIG_DIR")+"/configuration.properties");
			prop.load(input);
			uploadType=prop.getProperty("s3UploadType");
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	public static void main(String a[]){
		getCompanyProperties();
	}


	public static void getCompanyProperties(){
		System.out.println("=====Inside getCompanyProperties=====");
		Connection conn=null;
		java.sql.PreparedStatement st=null;
		ResultSet rs=null;
		List<ChannelEmailDetailsPerCompanyVO> companyPropList=null;
		try{
			conn=getConnection();



			String sql="select * from channel_email_details_per_company";

			st=conn.prepareStatement(sql);


			rs=st.executeQuery();



			String query = "update channel_email_details_per_company set support_email_is_changed = ? ";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, 0);
			preparedStmt.executeUpdate();

			if(rs!=null){
				companyPropList=new ArrayList<ChannelEmailDetailsPerCompanyVO>();
				while(rs.next()){

					ChannelEmailDetailsPerCompanyVO compropObj=new ChannelEmailDetailsPerCompanyVO();

					compropObj.setChannelEmailDetailsId(Integer.valueOf(rs.getString("channel_email_details_id")));
					compropObj.setCompanyId(Integer.valueOf(rs.getString("company_id")));
					compropObj.setSupportEmailName(rs.getString("support_email_name"));
					compropObj.setSupportEmailAddress(rs.getString("support_email_address"));
					compropObj.setForwardingSupportEmailAddress(rs.getString("forwarding_support_email_address"));
					compropObj.setForwardingSupportEmailPassword(rs.getString("forwarding_support_email_password"));
					companyPropList.add(compropObj);

				}
			}
			System.out.println("=====read getCompanyProperties=====size"+companyPropList.size());
			if(companyPropList!=null && companyPropList.size()>0){
				generateXMLForCompany(companyPropList);
			}


		}catch(Exception e){
			e.printStackTrace();
		}finally {
			if(st!=null){
				try {
					st.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}




	public static void generateXMLForCompany(List<ChannelEmailDetailsPerCompanyVO> companyPropList){
		System.out.println("===== generateXMLForCompany=====size"+companyPropList.size());

		try{
			if(companyPropList!=null && companyPropList.size()>0){

				DocumentBuilderFactory dbFactory =DocumentBuilderFactory.newInstance();
				dbFactory.setNamespaceAware(true);
				DocumentBuilder dBuilder =   dbFactory.newDocumentBuilder();
				Document doc = dBuilder.newDocument();



				// root element
				Element rootElement = doc.createElement("beans");

				//rootNode.createElementNS("http://example/namespace", "PREFIX:aNodeName");
				doc.appendChild(rootElement);





				rootElement.setAttribute("xmlns","http://www.springframework.org/schema/beans");
				rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
				rootElement.setAttribute("xmlns:context","http://www.springframework.org/schema/context");
				rootElement.setAttribute("xmlns:int","http://www.springframework.org/schema/integration");
				rootElement.setAttribute("xmlns:int-mail","http://www.springframework.org/schema/integration/mail");
				rootElement.setAttribute("xmlns:util","http://www.springframework.org/schema/util");

				rootElement.setAttribute("xsi:schemaLocation","http://www.springframework.org/schema/integration http://www.springframework.org/schema/integration/spring-integration.xsd http://www.springframework.org/schema/beans "+
						" http://www.springframework.org/schema/beans/spring-beans.xsd "+
						" http://www.springframework.org/schema/integration/mail http://www.springframework.org/schema/integration/mail/spring-integration-mail.xsd "+
						" http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd "+
						" http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd");





				Element contextComponent=doc.createElementNS("http://www.springframework.org/schema/context", "context:component-scan");
				contextComponent.setAttribute("base-package", "com.issue.email");
				rootElement.appendChild(contextComponent);

				Element contextProperty=doc.createElementNS("http://www.springframework.org/schema/context", "context:property-placeholder");
				contextProperty.setAttribute("location", "classpath:mail.properties");

				rootElement.appendChild(contextProperty);



				Element configBean=doc.createElement("bean");
				configBean.setAttribute("id", "configuration");
				configBean.setAttribute("class", "org.apache.commons.configuration.CompositeConfiguration");

				Element constructor=doc.createElement("constructor-arg");

				Element list=doc.createElement("list");

				Element beanInside=doc.createElement("bean");	         
				beanInside.setAttribute("class", "org.apache.commons.configuration.PropertiesConfiguration");

				Element constructorInside=doc.createElement("constructor-arg");
				constructorInside.setAttribute("type", "java.net.URL");
				Element value=doc.createElement("value");
				value.setTextContent("classpath:mail.properties");
				constructorInside.appendChild(value);

				beanInside.appendChild(constructorInside);

				list.appendChild(beanInside);

				constructor.appendChild(list);
				configBean.appendChild(constructor);

				rootElement.appendChild(configBean);

				for(ChannelEmailDetailsPerCompanyVO eachCompanyChannels:companyPropList){
					Element messageHandlerBean=doc.createElement("bean");
					messageHandlerBean.setAttribute("id", "messageHandler_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					messageHandlerBean.setAttribute("class", "com.issue.email.pop3.handler.MailMessageHandler");


					Element property1=doc.createElement("property");
					property1.setAttribute("name", "emailSource");
					property1.setAttribute("value", eachCompanyChannels.getSupportEmailAddress());


					Element property2=doc.createElement("property");
					property2.setAttribute("name", "companyId");
					property2.setAttribute("value", String.valueOf(eachCompanyChannels.getCompanyId()));


					Element property3=doc.createElement("property");
					property3.setAttribute("name", "forwardEmailAddress");
					property3.setAttribute("value", eachCompanyChannels.getForwardingSupportEmailAddress());

					Element property4=doc.createElement("property");
					property4.setAttribute("name", "forwardEmailAddressPassword");
					property4.setAttribute("value", eachCompanyChannels.getForwardingSupportEmailPassword());

					messageHandlerBean.appendChild(property1);
					messageHandlerBean.appendChild(property2);
					messageHandlerBean.appendChild(property3);
					messageHandlerBean.appendChild(property4);

					rootElement.appendChild(messageHandlerBean);


					Element intAdapter=doc.createElementNS("http://www.springframework.org/schema/integration", "int:channel");
					intAdapter.setAttribute("id","receiveChannel_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					rootElement.appendChild(intAdapter);

					String forwardAddress=eachCompanyChannels.getForwardingSupportEmailAddress();
					forwardAddress=forwardAddress.replace("@", "%40");
					String pop3Address="imaps://"+forwardAddress+":"+eachCompanyChannels.getForwardingSupportEmailPassword()+"@imap.gmail.com/INBOX";

					Element intmail=doc.createElementNS("http://www.springframework.org/schema/integration/mail", "int-mail:inbound-channel-adapter");
					intmail.setAttribute("id", "pop3ShouldDeleteTrue_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					intmail.setAttribute("store-uri", pop3Address);
					intmail.setAttribute("channel", "receiveChannel_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					intmail.setAttribute("should-delete-messages", "false");
					intmail.setAttribute("should-mark-messages-as-read", "true");
					
					intmail.setAttribute("auto-startup", "true");
					intmail.setAttribute("java-mail-properties", "javaMailProperties_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());



					Element intPoller=doc.createElementNS("http://www.springframework.org/schema/integration", "int:poller");
					intPoller.setAttribute("max-messages-per-poll", "10");
					intPoller.setAttribute("fixed-delay", "1000");

					intmail.appendChild(intPoller);
					rootElement.appendChild(intmail);


					Element utilProp=doc.createElementNS("http://www.springframework.org/schema/util", "util:properties");
					utilProp.setAttribute("id", "javaMailProperties_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					Element prop=doc.createElement("prop");
					prop.setAttribute("key", "mail.debug");
					prop.setTextContent("false");
					utilProp.appendChild(prop);
					
					Element socketFactory=doc.createElement("prop");
					socketFactory.setAttribute("key", "mail.imaps.socketFactory.class");
					socketFactory.setTextContent("javax.net.ssl.SSLSocketFactory");
					utilProp.appendChild(socketFactory);
					
					Element propStarttls=doc.createElement("prop");
					propStarttls.setAttribute("key", "mail.imap.starttls.enable");
					propStarttls.setTextContent("true");					
					utilProp.appendChild(propStarttls);
			    	
			    	Element propFallback=doc.createElement("prop");
			    	propFallback.setAttribute("key", "mail.imaps.socketFactory.fallback");
			    	propFallback.setTextContent("false");
			    	utilProp.appendChild(propFallback);
			    	
			    	Element propProtocol=doc.createElement("prop");
			    	propProtocol.setAttribute("key", "mail.store.protocol");
			    	propProtocol.setTextContent("imaps");	
			    	utilProp.appendChild(propProtocol);
			    	
					
					
					rootElement.appendChild(utilProp);


					Element serviceActivater=doc.createElementNS("http://www.springframework.org/schema/integration", "int:service-activator");
					serviceActivater.setAttribute("id", "emailActivator_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					serviceActivater.setAttribute("ref", "messageHandler_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					serviceActivater.setAttribute("input-channel", "receiveChannel_"+eachCompanyChannels.getCompanyId()+"_"+eachCompanyChannels.getChannelEmailDetailsId());
					serviceActivater.setAttribute("method", "handleMessage");





					rootElement.appendChild(serviceActivater);
				}










				// write the content into xml file
				TransformerFactory transformerFactory =     TransformerFactory.newInstance();
				Transformer transformer =  transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result =  new StreamResult(new File("/issuetracker/config/spring-integration-context.xml"));
				transformer.transform(source, result);
				System.out.println("===== generated XML=====");
				Thread.sleep(5000);
				StorageService awsS3Impl = new AmazonS3ServiceImpl();

				//Check the type of file upload
				if(uploadType!=null && uploadType.equalsIgnoreCase("shFile")){
					awsS3Impl.uploadThroughShell("/issuetracker/config/spring-integration-context.xml");
				}else{
					awsS3Impl.uploadNewFile("/issuetracker/config/spring-integration-context.xml", "spring-integration-context.xml");
				}
				System.out.println("===== Uploaded XML=====");


			}

		}catch(Exception e){
			e.printStackTrace();
		}

	}




	@Override
	public void generateXML() {
		// TODO Auto-generated method stub
		getCompanyProperties();
	}




	@Override
	public void generatePropertyFile() {
		// TODO Auto-generated method stub

	}

}
