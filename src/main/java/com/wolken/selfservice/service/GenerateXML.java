package com.wolken.selfservice.service;

public interface GenerateXML {
	
	public void generateXML();
	
	public void generatePropertyFile();

}
