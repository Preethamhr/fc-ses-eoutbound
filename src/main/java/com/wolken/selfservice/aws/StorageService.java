package com.wolken.selfservice.aws;

import java.io.InputStream;

public interface StorageService {
	
	/**
	 * Upload a file to cloud
	 * @param filePath
	 * @param objectKey
	 * @return boolean
	 * @throws WALSException: If failed to upload given file to cloud 
	 */
	boolean uploadNewFile(String filePath,String objectKey);
	
	/**
	 * Get pre-signed URL for a given file in cloud
	 * @param objectKey
	 * @return String
	 * @throws WALSException: If failed to get URL from cloud
	 */
    String getPreSignedURL(String objectKey);
    
    /**
	 * Get file content for a given file
	 * @param objectKey
	 * @return InputStream
	 * @throws WALSException: If failed to get content from cloud
	 */
    InputStream getFileContent(String objectKey);
    
    /**
     * Method to upload the attachments using shell script
     * @param filePath
     */
    public void uploadThroughShell(String filePath);
	

}
