package com.wolken.selfservice.aws;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;

public class AmazonS3ServiceImpl implements StorageService {

	
	
	private AmazonS3 s3;
	
	
	/**
	 * Bucket name in Amazon s3 server
	 */
	
    //private String bucket="fcemail-attachments";
	//private String bucket="fc.cs.email.prod.attachments";
	private String bucket="selfservice-wolken";
						   
	private long expiration=1200000;
	
	private String s3AccessKey="AKIAIMTX3W5XPF3WVYFA";

	private String s3AccessSecret="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";
	

	public AmazonS3ServiceImpl() {
		//default empty constructor
		try {
			init();
			//s3 = new AmazonS3Client();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//@PostConstruct
	/**
	 * Initialization of the services post construction
	 * @throws Exception
	 */
	public void init() throws Exception {
		s3 = new AmazonS3Client(new BasicAWSCredentials(s3AccessKey, s3AccessSecret) );
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	/**
	 * Upload a file to Amazon s3 server
	 * 
	 * @param filePath
	 * @param amazonS3ObjectKey
	 * @return boolean
	 * @throws WALSException
	 *             : If failed to upload given file to Amazon s3 server
	 */
	@Override
	public boolean uploadNewFile(String filePath, String amazonS3ObjectKey) {
		boolean status = false;
		try {
			
			PutObjectResult result = s3.putObject(bucket, amazonS3ObjectKey,
					new File(filePath));
			s3.setObjectAcl(bucket, amazonS3ObjectKey, CannedAccessControlList.AuthenticatedRead);
			if (result != null) {
				status = true;
			}
		} catch (AmazonClientException exception) {
			status = false;
			exception.printStackTrace();
		}
		return status;
	}

	/**
	 * Get pre-signed URL for a given file in Amazon s3 server
	 * @param amazonS3ObjectKey
	 * @return String
	 * @throws WALSException
	 *             : If failed to get URL from Amazon s3 server
	 */
	@Override
	public String getPreSignedURL(String amazonS3ObjectKey)
			  {
		String preSignedURL = null;
		try {
			
			long expirationTime = System.currentTimeMillis();
			expirationTime += expiration;
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
					bucket, amazonS3ObjectKey);
			generatePresignedUrlRequest.setMethod(HttpMethod.GET);
			generatePresignedUrlRequest.setExpiration(new Date(expirationTime));
			URL url = s3.generatePresignedUrl(generatePresignedUrlRequest);
			preSignedURL = url.toString();
		} catch (AmazonServiceException exception) {
			

		} catch (AmazonClientException ace) {
			
		}
		return preSignedURL;
	}

	/**
	 * Get file content for a given file
	 * 
	 * @param amazonS3ObjectKey
	 * @return InputStream
	 * @throws WALSException
	 *             : If failed to get content from Amazon s3 server
	 */
	@Override
	public InputStream getFileContent(String amazonS3ObjectKey)
			 {
		InputStream objectData = null;
		try {
			S3Object object = s3.getObject(new GetObjectRequest(bucket,
					amazonS3ObjectKey));
			objectData = object.getObjectContent();
		} catch (AmazonServiceException exception) {

		} catch (AmazonClientException ace) {
		}
		return objectData;
	}
	
	
	
	public void uploadThroughShell(String filePath){
		 Runtime r = Runtime.getRuntime();
		 try {
	        Process p = null;
	       // String cmd[] = {"./test.sh","/var/cache/tomcat6/temp/1474488310360__LogMeIn.txt"};
	        //System.out.println("File path of the file "+filePath);
	        
	        String[] cmd = {"sh",  "/issuetracker/config/uploadS3.sh", filePath};
	        
	            p = r.exec(cmd);
	            
	            BufferedReader stdInput = new BufferedReader(new
	                    InputStreamReader(p.getInputStream()));
	            String s;                                                                
	            while ((s = stdInput.readLine()) != null) {                                
	             // System.out.println("Upload status output: " + s);                             
	            }   
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}
}
