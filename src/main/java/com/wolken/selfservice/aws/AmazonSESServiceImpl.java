package com.wolken.selfservice.aws;

import java.io.FileNotFoundException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AWSJavaMailTransport;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.wolken.selfservice.jdbc.handler.MailInfo;
import com.wolken.selfservice.mail.helper.PropertiesHelper;


public class AmazonSESServiceImpl {
	
	
	private static Session session = null;	
	private static Transport transport = null;
	
	private static AmazonSimpleEmailService sesclient=null;
	
	 public static AmazonSimpleEmailService getSesClient(){
	    	if(sesclient==null){
	    		try {
					loadAWSSESSessionAndTransport();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	
	    	return sesclient;
	    }
	
	public static synchronized void loadAWSSESSessionAndTransport() throws MessagingException {
		System.out.println(" Inside loadAWSSESSessionAndTransport session "+session);
		try {
			
			Properties prop=PropertiesHelper.getAwsCredentialProperties();
			//if(session == null) {
				session = null;
				//AWSCredentials credentials = new BasicAWSCredentials("AKIAIO5K5P3THGQUULQQ", "bHXlioTjvNFVdAqGEEH7PkQilkfOJ5sSSAOMXWN/");
				//AWSCredentials credentials = new BasicAWSCredentials("AKIAIIAPT2ANFD6MTGVA", "wuc2Z+SAHQw6m9qwYs23B8sTPtsECoTZ9zoDYfCX");
				AWSCredentials credentials = new BasicAWSCredentials(prop.getProperty("accessKey"), prop.getProperty("secretKey"));
				System.out.println(" Inside loadAWSSESSessionAndTransport credentials "+credentials);
//				AWSCredentials credentials = new ClasspathPropertiesFileCredentialsProvider().getCredentials();
				AmazonSimpleEmailService ses = new AmazonSimpleEmailServiceClient(credentials);
				System.out.println(" Inside loadAWSSESSessionAndTransport ses "+ses);
				Region usWest2 = Region.getRegion(Regions.US_WEST_2);
				System.out.println(Regions.US_WEST_2+" Inside loadAWSSESSessionAndTransport usWest2 "+usWest2);
				ses.setRegion(usWest2);
				/*
				 * Setup JavaMail to use the Amazon Simple Email Service by specifying
				 * the "aws" protocol.
				 */
				if(sesclient==null){
					sesclient = new AmazonSimpleEmailServiceClient(credentials);
					System.out.println(" Inside loadAWSSESSessionAndTransport ses "+ses);
					Region usWest2Region = Region.getRegion(Regions.US_WEST_2);
					System.out.println(Regions.US_WEST_2+" Inside loadAWSSESSessionAndTransport usWest2 "+usWest2);
					sesclient.setRegion(usWest2Region);
				}
				
				
				
				Properties props = new Properties();
				props.setProperty("mail.transport.protocol", "aws");
				props.setProperty("mail.aws.region", "US-WEST-2");
		        /*
		         * Setting mail.aws.user and mail.aws.password are optional. Setting
		         * these will allow you to send mail using the static transport send()
		         * convince method.  It will also allow you to call connect() with no
		         * parameters. Otherwise, a user name and password must be specified
		         * in connect.
		         */
		        props.setProperty("mail.aws.user", credentials.getAWSAccessKeyId());
		        props.setProperty("mail.aws.password", credentials.getAWSSecretKey());
		        
		        session = Session.getInstance(props);
		        System.out.println(" Inside loadAWSSESSessionAndTransport session 2 "+session);
		        
		        transport = new AWSJavaMailTransport(session, null);
		        transport.connect();
		        System.out.println(" Inside loadAWSSESSessionAndTransport transport "+transport);
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		
	}
	
	protected void finalize() throws Throwable {
		if(transport != null) {
			transport.close();
		}
		super.finalize();
	}
	
	

	
	public static void transportMessage(Message message) throws MessagingException {
		System.out.println(" transport.isConnected() "+transport.isConnected());
		if( ! transport.isConnected()) {
			loadAWSSESSessionAndTransport();
		}
		transport.sendMessage(message, null);
	}


	public static Message createEmailMessage(MailInfo mailInfo) throws MessagingException,AddressException, FileNotFoundException {
		Message message = null;
		try {
			System.out.println(" Inside createEmailMessage session 1 "+session);
			if(session == null) {
				loadAWSSESSessionAndTransport();
			}
			System.out.println(" Inside createEmailMessage  session 2 "+session);
			// Create a new Message
			message = new MimeMessage(session);
			message.setFrom(new InternetAddress((mailInfo.getEmail_from()==null ||mailInfo.getEmail_from().trim().equalsIgnoreCase("")) ?
					PropertiesHelper.getProperties().getProperty("emailfrom"):mailInfo.getEmail_from()));

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(mailInfo.getEmail_to()));

			if (mailInfo.getEmail_cc() != null
					&& !mailInfo.getEmail_cc().isEmpty()) {
				message.setRecipients(Message.RecipientType.CC,
						InternetAddress.parse(mailInfo.getEmail_cc()));
			}

			//Add BCC Address
			if (mailInfo.getEmail_bcc() != null
					&& !mailInfo.getEmail_bcc().isEmpty()) {
				message.setRecipients(Message.RecipientType.BCC,
						InternetAddress.parse(mailInfo.getEmail_bcc()));
			}
			
			
			Address[] addresses = new Address[1];
			addresses[0] = new InternetAddress((mailInfo.getEmail_from()==null ||mailInfo.getEmail_from().trim().equalsIgnoreCase("")) ?
					PropertiesHelper.getProperties().getProperty("emailfrom"):mailInfo.getEmail_from());

			message.setReplyTo(addresses);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return message;
	}

}
