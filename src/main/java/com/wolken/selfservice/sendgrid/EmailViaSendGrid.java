package com.wolken.selfservice.sendgrid;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.wolken.selfservice.aws.AmazonSESServiceImpl;
import com.wolken.selfservice.jdbc.handler.MailInfo;
import com.amazonaws.services.simpleemail.model.RawMessage;

public class EmailViaSendGrid {
	
	
	public static final String apiKey2="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";
	
	public static void sendMailFromSendGrid(String to,String cc,String bcc,String from,String fromName,String subject,String body, String attachments){
		String [] emails = null;
		Email emailObject = null;
		Mail mail = null;
		Personalization personalization = null;
		Content content = null;
		try {
			SendGrid sg = new SendGrid(apiKey2);
			Request request = new Request();
			
			mail = new Mail();
			personalization = new Personalization();
			
			content = new Content("text/html", body);
			mail.addContent(content);
			mail.setSubject(subject);
			
			if(from!=null){
				emailObject = new Email();
				emailObject.setEmail(from);
				if(fromName!=null)
					emailObject.setName(fromName);
				mail.setFrom(emailObject);
			}
			if(to!=null && !to.trim().equals("")){
				emails = to.split(",");
				for(int i=0;i<emails.length;i++){
					emailObject = new Email();
					emailObject.setEmail(emails[i]);
					personalization.addTo(emailObject);	
				}	
			}
			if(cc!=null && !cc.trim().equals("")){
				emails = cc.split(",");
				for(int i=0;i<emails.length;i++){
					emailObject = new Email();
					emailObject.setEmail(emails[i]);
					personalization.addCc(emailObject);	
				}	
			}
			if(bcc!=null && !bcc.trim().equals("")){
				emails = bcc.split(",");
				for(int i=0;i<emails.length;i++){
					emailObject = new Email();
					emailObject.setEmail(emails[i]);
					personalization.addBcc(emailObject);	
				}	
			}
			
			mail.addPersonalization(personalization);
			
			if (attachments != null	&& attachments.trim().length() > 0) {
				String[] filesArray = attachments.split("##");
				for(int i=0; i<filesArray.length;i++){
					InputStream inputStream = null;
					URI uri = new URI(filesArray[i]);
					try{
						URL url = uri.toURL(); //get URL from your uri object
						inputStream = url.openStream();
						
						byte[] fileData = null;
						try {
							fileData = org.apache.commons.io.IOUtils.toByteArray(inputStream);
						} catch (IOException ex) {
							ex.printStackTrace();
						}

						Base64 x = new Base64();
						String data = x.encodeAsString(fileData);

						Attachments attachment = new Attachments();
						attachment.setFilename(filesArray[i]);
						attachment.setContent(data);
						attachment.setDisposition("attachment");
						mail.addAttachments(attachment);
					}catch (Exception e) {
						e.printStackTrace();
					}finally {
						if (inputStream != null) {
							try {
								inputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}

					}
				}
			}
			
			request.method = Method.POST;
			request.endpoint = "mail/send";
			request.body = mail.build();
			Response response = sg.api(request);
			System.out.println("sendgrid: response status code: "+response.statusCode);
			System.out.println("sendgrid: body: "+response.body);
			System.out.println("sendgrid: headers: "+response.headers);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sendMailFromSendGrid(String to,String from,String subject,String body, String attachments){
		//String apiKey2="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";
		SendGrid sg = new SendGrid(apiKey2);
		Request request = new Request();
		try {
			Email fromEmail = new Email(from);
			Email toEmail = new Email(to);
			Content content = new Content("text/html", body);
			Mail mail = new Mail(fromEmail, subject, toEmail, content);
			
			if (attachments != null
					&& attachments.trim().length() > 0) {
				String[] filesArray = attachments.split("##");
				
				for(int i=0; i<filesArray.length;i++){
					
					InputStream inputStream = null;
					URI uri = new URI(filesArray[i]);
					try{
						URL url = uri.toURL(); //get URL from your uri object
						inputStream = url.openStream();

					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}finally {
						if (inputStream != null) {
							try {
								inputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}

					}



					byte[] fileData = null;
					try {
						fileData = org.apache.commons.io.IOUtils.toByteArray(inputStream);
					} catch (IOException ex) {
						ex.printStackTrace();
					}



					Base64 x = new Base64();
					String data = x.encodeAsString(fileData);

					Attachments attachment = new Attachments();
					attachment.setFilename(filesArray[i]);
					attachment.setContent(data);
					attachment.setDisposition("attachment");
					//attachment.setType("image/png");
					mail.addAttachments(attachment);
					
				}
				
			}
			
			request.method = Method.POST;
			request.endpoint = "mail/send";
			request.body = mail.build();
			Response response = sg.api(request);
			System.out.println(response.statusCode);
			System.out.println(response.body);
			System.out.println(response.headers);
		} catch (IOException ex) {
			ex.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/* sending mails from https://sendgrid.com
	 * userId: padmarag@wolkensoftware.com
	 * password: new@1234
	 * check email activity: https://app.sendgrid.com/email_activity
	 * key: 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
	 * Using 30-day trial that sends 40000 e-mails.
	 * Registered for trial on 23-march-2017
	 * https://sendgrid.com/pricing/
	 * */
	public static void sendMailFromSendGridNotification(String to,String from,String subject,String body){
		String apiKey2="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";
		SendGrid sg = new SendGrid(apiKey2);
		Request request = new Request();
		Email toEmail=null;
		try {
			Email fromEmail = new Email(from);
			Content content = new Content("text/html", body);
			Personalization p1 = new Personalization();
			p1.setSubject(subject);
			
			String[] tomails = to.split(",");
			for(int i=0;i<tomails.length;i++){
				toEmail = new Email(tomails[i].trim());
				p1.addTo(toEmail);
			}
			
			Mail mail = new Mail();
			mail.addContent(content);
			mail.from = fromEmail;
			
			mail.addPersonalization(p1);
			
			request.method = Method.POST;
			request.endpoint = "mail/send";
			request.body = mail.build();
			Response response = sg.api(request);
			
			System.out.println("response status: "+response.statusCode);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws AddressException, FileNotFoundException, MessagingException, URISyntaxException {
		MailInfo info = new MailInfo();
		info.setAttachPath("1600763567939__response_and_resolution_time_issue2.png##");
		info.setEmail_cc("gajananmg2013@gmail.com");
		info.setEmail_from("uat.technicalsupport@broadcom.com");
		info.setEmail_request_info("<div style=\"display: none;\" class=\"wolken_outbound\"></div><p>&nbsp;</p>\r\n" + 
				" <p>&nbsp;</p>\r\n" + 
				" <p>Testing attachments</p>\r\n" + 
				" <hr />\r\n" + 
				" <div class=\"wolken_outbound\" style=\"display: none;\">&nbsp;</div>\r\n" + 
				" <p>Testing Outbound</p>");
		info.setEmail_to("gajanan@wolkensoftware.com");
		info.setSubject("Wolken - eoutbound Testing Mail");
		sendMailFromSES(info);
	}
	
	
	public static void sendMailFromSES(MailInfo mailInfo) throws AddressException, FileNotFoundException, MessagingException, URISyntaxException{
		System.out.println("Start sendMailFromSES ");
		try{
			Message message = AmazonSESServiceImpl.createEmailMessage(mailInfo);
			message.setSubject(mailInfo.getSubject()==null ? " ":mailInfo.getSubject());
			Multipart multipart = new MimeMultipart();
			if (mailInfo.getAttachPath() != null
					&& mailInfo.getAttachPath().trim().length() > 0) {
				System.out.println(" *** Inside mailInfo.getAttachPath() *** ");
				String[] filesArray = mailInfo.getAttachPath().split("##");
				
				for (int i = 0; i < filesArray.length; i++) {
					System.out.println(" *** filesArray[i] *** "
							+ filesArray[i]);
					InputStream inputStream = null;
					OutputStream outputStream = null;
					File file = new File("/issuetracker/emailattachments/"
							+ filesArray[i]);

					// Staging Paths
					  String attachmetName = "https://s3-ap-south-1.amazonaws.com/fc.stg.attachments/" +filesArray[i]; 
					  String attachmentPath = "http://fc-staging-services.wolkencare.com/crm-issuetracker/getS3?url=" +attachmetName;
					 
					// Production Paths
				/*	String attachmetName = "https://s3-ap-south-1.amazonaws.com/fc.cs.email.prod.attachments/"
							+ filesArray[i];
					String attachmentPath = "https://wolken-services.freecharge.in/crm-issuetracker/getS3?url="
							+ attachmetName;
							*/

					URI uri = new URI(attachmentPath);
					try {
						URL url = uri.toURL(); // get URL from your uri object
						inputStream = url.openStream();
						System.out.println(" *** after inputStream *** ");
						outputStream = new FileOutputStream(file);
						System.out.println(" *** after outputStream *** ");
						int read = 0;
						byte[] bytes = new byte[1024];
						while ((read = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, read);
						}
						System.out.println(" *** after while *** ");

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						if (inputStream != null) {
							try {
								inputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						if (outputStream != null) {
							try {
								outputStream.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					MimeBodyPart attachmentPart = new MimeBodyPart();
					FileDataSource fileDataSource = new FileDataSource(file) {
						@Override
						public String getContentType() {
							return "application/octet-stream";
						}
					};
					attachmentPart.setDataHandler(new DataHandler(fileDataSource));
					attachmentPart.setFileName(fileDataSource.getName());
					multipart.addBodyPart(attachmentPart);

					System.out.println(" *** fileDataSource.getName() *** "	+ fileDataSource.getName());
				}
				//multipart.addBodyPart(messagePart);
				//message.setContent(multipart);
			}
				base64ImageConverter(mailInfo, message, multipart);
				AmazonSimpleEmailService ses=AmazonSESServiceImpl.getSesClient();
				
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	            message.writeTo(outputStream);
	            RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
	            SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
	            ses.sendRawEmail(rawEmailRequest);
			
			
			System.out.println("mail sent to -->"+mailInfo.getEmail_to() +" with subject-->"+mailInfo.getSubject());

		}catch(Exception e){
			
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		System.out.println("end sendMailFromSES ");
	}
	
	private static final Pattern imgRegExp = Pattern
			.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>");

	public static void base64ImageConverter(MailInfo mailInfo, Message message,
			Multipart mmp) {
		System.out.println("reached base64ImageConverter");
		try {
			Map<String, String> inlineImage = new HashMap<String, String>();
			String body = mailInfo.getEmail_request_info();
			final Matcher matcher = imgRegExp.matcher(body);
			int i = 0;
			while (matcher.find()) {
				String src = matcher.group();
				if (body.indexOf(src) != -1) {
					String srcToken = null;
					int x = 0, y = 0;
					if (src.contains("src=\"")) {
						srcToken = "src=\"";
						x = src.indexOf(srcToken);
						y = src.indexOf("\"", x + srcToken.length());
					} else {
						srcToken = "src='";
						x = src.indexOf(srcToken);
						y = src.indexOf("'", x + srcToken.length());
					}
					if (x < y) {
						String srcText = src
								.substring(x + srcToken.length(), y);
						String cid = "image" + i;
						String newSrc = src.replace(srcText, "cid:" + cid);
						System.out.println("Splitted Values : "+srcText.split(","));
						if(srcText.split(",").length > 1){
							inlineImage.put(cid, srcText.split(",")[1]);
						}
						body = body.replace(src, newSrc);
					} else {
						System.out.println("lengths x: " + x + " y: " + y
								+ " srcToken.length() : " + srcToken.length());
						System.out.println("src doesnot contain source path");
					}
					i++;

				}
			}
			MimeBodyPart messagePart = new MimeBodyPart();
			messagePart.setText(body, "utf-8", "html");

			mmp.addBodyPart(messagePart);
			Iterator<Entry<String, String>> it = inlineImage.entrySet()
					.iterator();
			while (it.hasNext()) {
				Entry<String, String> pairs = it.next();
				PreencodedMimeBodyPart pmp = new PreencodedMimeBodyPart(
						"base64");
				pmp.setHeader("Content-ID", "<" + pairs.getKey() + ">");
				pmp.setDisposition(MimeBodyPart.INLINE);
				pmp.setContent(pairs.getValue(), "image/png");
				mmp.addBodyPart(pmp);
			}

			message.setContent(mmp);
			System.out.println("End of base64ImageConverter ");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
