/**
 * 
 */
package com.wolken.selfservice.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Raghavendra
 *
 */
public class ConfigurationPropertiesUtil {
	
	
	
	//added by Keerthi
		public static final String appliactionHostedDomain;
		public static final String s3UrlPath;//Hopefully this will work, If any error pls talk to Raghavendra
		public static final String sseHostedURL;
		public static final String redisHostUrl;
		public static final String jwtSecretKey;
		public static final String crmServiceHostedURL;
		public static final String signupServiceHostedURL;
		public static final String openIssueDetailsSentForApprovalURL;
		public static final String elasticsearchURL;
		public static final String s3UploadType;
		public static final String ticketCreationHostURL;
		public static final String sentimentAnalysisURL;
		static{
			String appHostDomain=null;
			String s3path=null;
			String ssePath=null; 
			String redisPath=null;
			String jwtKey=null;	
			String crmServicePath=null;
			String signupUrl=null;
			Properties prop = new Properties();
			InputStream input = null;
			String sentForApprovalURL=null;
			String esURL=null;
			String uploadType=null;
			String ticketCreationHostPath=null;
			String sentimentAnalysisUrl=null;
			try {
				input = new FileInputStream(System.getenv("CONFIG_DIR")+"/configuration.properties");
				prop.load(input);
				s3path = prop.getProperty("s3BucketUrl");
				ssePath= prop.getProperty("sse.host");
				redisPath=prop.getProperty("host.redis.connectivity.ip");
				jwtKey=prop.getProperty("jwt.secret.key");
				crmServicePath=prop.getProperty("host.crmissuetracker.connectivity.ip");
				signupUrl=prop.getProperty("host.signup.connectivity.ip");
				appHostDomain=prop.getProperty("application.host.domain");
				sentForApprovalURL=prop.getProperty("issueDetailsForSentForApprovalURL");
				esURL=prop.getProperty("elasticsearchURL");
				uploadType=prop.getProperty("s3UploadType");
				ticketCreationHostPath=prop.getProperty("ticket.creation.host.ip");
				sentimentAnalysisUrl=prop.getProperty("sentiment.analysis.url");
			}catch (Exception e) {
				
				e.printStackTrace();
				throw new ExceptionInInitializerError(e);
			}	
			finally {
				if(input!= null){	
					try {
						input.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				appliactionHostedDomain=appHostDomain;
				s3UrlPath=s3path;
				sseHostedURL=ssePath;
				redisHostUrl=redisPath;
				jwtSecretKey=jwtKey;
				crmServiceHostedURL=crmServicePath;
				signupServiceHostedURL=signupUrl;
				openIssueDetailsSentForApprovalURL = sentForApprovalURL;
				elasticsearchURL=esURL;
				s3UploadType=uploadType;
				ticketCreationHostURL=ticketCreationHostPath;
				sentimentAnalysisURL=sentimentAnalysisUrl;
			}
		}
		
		
		
		//end By keerthi
	
	

}
