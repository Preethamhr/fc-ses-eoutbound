package com.wolken.selfservice.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import com.google.gson.Gson;
import com.wolken.selfservice.model.ThreadCheckResponse;

public class RestUtil {
	
	
	public static String httpGetCall(String urlEndPoint){
		
		String output = null;
		try {

			URL url = new URL(urlEndPoint);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String stream;
			System.out.println("Output from Server .... \n");
			while ((stream = br.readLine()) != null) {
				
				output = stream;
			}

			conn.disconnect();

		  } catch (MalformedURLException e) {
			  System.out.println("Exception in httpGetCall"+e.getMessage());
			e.printStackTrace();
			return output;

		  } catch (IOException e) {
			  System.out.println("Exception in httpGetCall"+e.getMessage());
			e.printStackTrace();
			return output;
		  }
		
		return output;
		
	}
	
	
	public static String httpPostCall(String url, String urlParams){

		HttpURLConnection connection = null;
	    OutputStreamWriter wr = null;
	    BufferedReader rd  = null;
	    StringBuilder sb = new StringBuilder("");
	    String line = null;

		try {
			System.out.println("url->"+url);
			System.out.println("urlParams->"+urlParams);
			URL serverAddress = new URL(url);

			connection = (HttpURLConnection)serverAddress.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			//connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
			
			DataOutputStream dwr = new DataOutputStream(connection.getOutputStream());
			dwr.writeBytes(urlParams);
			dwr.flush();
			dwr.close();
			
			connection.setReadTimeout(10000);
			connection.connect();

			rd = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			sb = new StringBuilder();
			while ((line = rd.readLine()) != null) {
				sb.append(line + '\n');
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
	
	public static void main(String[] args) {
		
		try{
			RestUtil rest = new RestUtil();
			String jsonString = rest.httpGetCall("https://csdemo.wolkenservicedesk.com/issuetracker/checktwitterthreads.do?twitterAccountPkey=4&userId=27740");
			System.out.println("JSON String coming here "+jsonString);
			Gson g = new Gson(); 
			ThreadCheckResponse obj = g.fromJson(jsonString, ThreadCheckResponse.class);
			 
			System.out.println("JSON returned thread status "+obj.isThreadstatus());
			System.out.println("JSON returned s a result "+obj.toString());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
