package com.wolken.selfservice.model;

public class ThreadCheckResponse {
	
	
	private boolean threadstatus;
	private String threadmessage;
	
	
	public String getThreadmessage() {
		return threadmessage;
	}
	public void setThreadmessage(String threadmessage) {
		this.threadmessage = threadmessage;
	}
	public boolean isThreadstatus() {
		return threadstatus;
	}
	public void setThreadstatus(boolean threadstatus) {
		this.threadstatus = threadstatus;
	}

	
	
}
