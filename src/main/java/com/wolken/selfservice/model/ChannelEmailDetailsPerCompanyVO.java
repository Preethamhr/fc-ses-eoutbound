package com.wolken.selfservice.model;

import java.util.Date;

public class ChannelEmailDetailsPerCompanyVO {
	

	private Integer channelEmailDetailsId;
	private String supportEmailName;	
	private String supportEmailAddress;	
	private String forwardingSupportEmailAddress;	
	private String forwardingSupportEmailPassword;	
	private boolean supportEmailIsActive;	
	private boolean supportEmailIsChanged;	
	private Integer issueSourceId;	
	private Integer companyId;	
	private Integer createdBy;	
	private Date createdDate;	
	private Integer updatedBy;	
	private Date updatedDate;
	/**
	 * @return the channelEmailDetailsId
	 */
	public Integer getChannelEmailDetailsId() {
		return channelEmailDetailsId;
	}
	/**
	 * @param channelEmailDetailsId the channelEmailDetailsId to set
	 */
	public void setChannelEmailDetailsId(Integer channelEmailDetailsId) {
		this.channelEmailDetailsId = channelEmailDetailsId;
	}
	/**
	 * @return the supportEmailName
	 */
	public String getSupportEmailName() {
		return supportEmailName;
	}
	/**
	 * @param supportEmailName the supportEmailName to set
	 */
	public void setSupportEmailName(String supportEmailName) {
		this.supportEmailName = supportEmailName;
	}
	/**
	 * @return the supportEmailAddress
	 */
	public String getSupportEmailAddress() {
		return supportEmailAddress;
	}
	/**
	 * @param supportEmailAddress the supportEmailAddress to set
	 */
	public void setSupportEmailAddress(String supportEmailAddress) {
		this.supportEmailAddress = supportEmailAddress;
	}
	/**
	 * @return the forwardingSupportEmailAddress
	 */
	public String getForwardingSupportEmailAddress() {
		return forwardingSupportEmailAddress;
	}
	/**
	 * @param forwardingSupportEmailAddress the forwardingSupportEmailAddress to set
	 */
	public void setForwardingSupportEmailAddress(String forwardingSupportEmailAddress) {
		this.forwardingSupportEmailAddress = forwardingSupportEmailAddress;
	}
	/**
	 * @return the forwardingSupportEmailPassword
	 */
	public String getForwardingSupportEmailPassword() {
		return forwardingSupportEmailPassword;
	}
	/**
	 * @param forwardingSupportEmailPassword the forwardingSupportEmailPassword to set
	 */
	public void setForwardingSupportEmailPassword(String forwardingSupportEmailPassword) {
		this.forwardingSupportEmailPassword = forwardingSupportEmailPassword;
	}
	/**
	 * @return the supportEmailIsActive
	 */
	public boolean isSupportEmailIsActive() {
		return supportEmailIsActive;
	}
	/**
	 * @param supportEmailIsActive the supportEmailIsActive to set
	 */
	public void setSupportEmailIsActive(boolean supportEmailIsActive) {
		this.supportEmailIsActive = supportEmailIsActive;
	}
	/**
	 * @return the supportEmailIsChanged
	 */
	public boolean isSupportEmailIsChanged() {
		return supportEmailIsChanged;
	}
	/**
	 * @param supportEmailIsChanged the supportEmailIsChanged to set
	 */
	public void setSupportEmailIsChanged(boolean supportEmailIsChanged) {
		this.supportEmailIsChanged = supportEmailIsChanged;
	}
	/**
	 * @return the issueSourceId
	 */
	public Integer getIssueSourceId() {
		return issueSourceId;
	}
	/**
	 * @param issueSourceId the issueSourceId to set
	 */
	public void setIssueSourceId(Integer issueSourceId) {
		this.issueSourceId = issueSourceId;
	}
	/**
	 * @return the companyId
	 */
	public Integer getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

}
