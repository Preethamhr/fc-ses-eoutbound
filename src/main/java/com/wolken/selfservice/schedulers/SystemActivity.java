package com.wolken.selfservice.schedulers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;


import com.wolken.selfservice.util.RestUtil;

public class SystemActivity {
	
	private static final Logger LOGGER = Logger.getLogger(SystemActivity.class);
	
	//@Value("${autoAssignmentUrl}")
	private String autoAssignmentUrl;
	
//	@Value("${rulesEngineUrl}")
	private String rulesEngineUrl;
	
	//@Scheduled(fixedDelay=30000)
	public void runSystemActivity(){
		
		System.out.println("Calling Auto Assignment URL:"+autoAssignmentUrl);
		RestUtil.httpGetCall(autoAssignmentUrl);
		System.out.println("Calling Rules Engine URL:"+rulesEngineUrl);
		RestUtil.httpGetCall(rulesEngineUrl);
		
		
	}
	

}
