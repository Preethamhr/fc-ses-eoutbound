/**
 * 
 */
package com.wolken.selfservice.mail.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Raghavendra
 *
 */
public class PropertiesHelper {

	
	
	public static Properties properties = null;
	static {
		properties = new Properties();
		InputStream inputStream=null;
		try {
			inputStream = PropertiesHelper.class.getClassLoader().getResourceAsStream("EmailConnection.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			System.out.println("Property file does not exist");
			throw new RuntimeException("Could not load email connection properties file",e);
		}finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static Properties getProperties() {
		return properties;
	}
	
	private static Properties companyProperties = null;
	static {
		companyProperties = new Properties();
		InputStream inputStream=null;
		try {
			inputStream = PropertiesHelper.class.getClassLoader().getResourceAsStream("Company.properties");
			companyProperties.load(inputStream);
		} catch (IOException e) {
			System.out.println("companyProperties file does not exist");
			throw new RuntimeException("Could not load companyProperties file",e);
		}
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static Properties getCompanyProperties() {
		return companyProperties;
	}
	
	
	
	private static Properties awsCredentialProperties = null;
	static {
		awsCredentialProperties = new Properties();
		InputStream inputStream=null;
		try {
			inputStream = PropertiesHelper.class.getClassLoader().getResourceAsStream("AwsCredentials.properties");
			awsCredentialProperties.load(inputStream);
		} catch (IOException e) {
			System.out.println("awsCredentialProperties file does not exist");
			throw new RuntimeException("Could not load awsCredentialProperties file",e);
		}finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static Properties getAwsCredentialProperties() {
		return awsCredentialProperties;
	}
}
